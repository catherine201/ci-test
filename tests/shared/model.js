import { Map } from 'immutable'
import ComponentModel from '@/model/ComponentModel'

const meta = Map({ id: 'componentap', type: 'componentType' })
// 这里的model 是kd 层控件需要传入的model，很多地方需要用到
export const model = (pageId = 'pageId', metaData = meta) => new ComponentModel(pageId, metaData, window.store.getState())
