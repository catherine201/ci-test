import React from 'react'
import { mount } from 'enzyme'

// eslint-disable-next-line jest/no-export
export default function mountTest (jsx) {
  describe('mount and unmount', () => {
    it('component could be updated and unmounted without errors', () => {
      const wrapper = mount(jsx())
      expect(() => {
        wrapper.setProps({})
        wrapper.unmount()
      }).not.toThrow()
    })
  })
}
