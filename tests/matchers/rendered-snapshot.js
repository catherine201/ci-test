import { mount, shallow } from 'enzyme'
// import { ReactElement } from 'react'

export function toMatchMountSnapshot (
  jsx
) {
  try {
    expect(mount(jsx).render()).toMatchSnapshot()

    return {
      message: () => 'expected JSX not to match snapshot',
      pass: true
    }
  } catch (e) {
    return {
      message: () => `expected JSX to match snapshot: ${e.message}`,
      pass: false
    }
  }
}

export function toMatchShallowSnapshot (
  jsx
) {
  try {
    expect(shallow(jsx).render()).toMatchSnapshot()

    return {
      message: () => 'expected JSX not to match snapshot',
      pass: true
    }
  } catch (e) {
    return {
      message: () => `expected JSX to match snapshot: ${e.message}`,
      pass: false
    }
  }
}
