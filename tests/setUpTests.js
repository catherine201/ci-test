import 'raf/polyfill' // 安装一下requestAnimation的模拟,react需要
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import * as jsdom from 'jsdom'
import 'whatwg-fetch'

const { window } = new jsdom.JSDOM('<!doctype html><html><body></body></html>')

global.window = window
global.spyApi = window.spyApi = {}
global.document = window.document
global.console = {
  log: console.log,
  warn: jest.fn(),
  error: console.error,
  info: console.info,
  debug: console.debug
}
// global.jsbarcode = global.jsbarcode = {}
configure({ adapter: new Adapter() })
