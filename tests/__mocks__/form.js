import { Map, List } from 'immutable'
export const form = (pageId) => {
  return {
    fid: '',
    pageId,
    config: Map({
      watermark: '',
      caption: Map({}),
      attachmentserver: '',
      openStyle: Map({}),
      imageUploadMaxSize: 1052428800,
      pageId,
      forbidfiletype: List([]),
      mobdomainurl: '',
      lang: 'zh_CN',
      setting: null,
      patchVersion: '',
      parentPageId: '',
      imgversion: '',
      version: '',
      type: '',
      fmtInfo: Map({ numFmtInfo: Map({}) }),
      appId: '',
      wsconfig: Map({ wsurl: '' }),
      formId: '',
      fileserver: ''
    }),
    data: Map({}),
    post: Map({}),
    meta: Map({})
  }
}
