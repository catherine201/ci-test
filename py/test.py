#! /usr/bin/env python
# This Python file uses the following encoding: utf-8
import re
import os
import urllib2
import json
import sys
import csv
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

def get_data(name, job_name, job_url, branch_name, shortcodenum, explain, phone, sonar_check_url):
    if job_name == 'bos-unit-test-merge-notify':
        return {
             "content":  '##' + '单元测试失败告警：' + '##' + '\n' +
                '负责人：%s' %name + '\n' +
                '提交分支：%s' %branch_name + '\n' +
                '提交SHA：%s' %shortcodenum + '\n' +
                '提交信息：%s' %explain + '\n' +
                '查看链接：%s' %job_url + '\n'
    ,
        "notifyParams": [
           {
            "type": "mobiles",
            "values": [
                phone
            ]
           }]
        }
    elif job_name == 'bos-daily-unit-test-notify':
        return {
             "content":  '##' + '单元测试失败告警：' + '##' + '\n' +
                '分支：%s' %branch_name + '\n' +
                '查看链接：%s' %job_url + '\n'
    ,
        "notifyParams": [
           {
            "type": "mobiles",
            "values": [
                '18892235646'
            ]
           }]
        }
    elif job_name == 'bos-sonarqube-check-notify':
        return {
             "content":  '##' + '代码扫描失败告警：' + '##' + '\n' +
                '负责人：%s' %name + '\n' +
                '提交分支：%s' %branch_name + '\n' +
                '提交SHA：%s' %shortcodenum + '\n' +
                '提交信息：%s' %explain + '\n' +
                '查看链接：%s' %sonar_check_url + '\n'
    ,
        "notifyParams": [
           {
            "type": "mobiles",
            "values": [
                phone
            ]
           }]
        }
    elif job_name == 'bos-daily-sonarqube-check-notify':
        return {
             "content":  '##' + '代码扫描失败告警：' + '##' + '\n' +
                '分支：%s' %branch_name + '\n' +
                '查看链接：%s' %sonar_check_url + '\n'
    ,
        "notifyParams": [
           {
            "type": "mobiles",
            "values": [
                phone
            ]
           }]
        }
    else:
        return None

def get_content():
    content = (os.popen("git log --pretty=format:\"%ae-&%h-&%s\" -1 ").read()).split("-&")
    return content


def post_yun_zhijia():
    data = []
    row1 = [1, 'a']
    row2 = [2, 'b']
    data.append(row1)
    data.append(row2)
    with open('https://gitee.com/yan-rubing/test/raw/6e0e24f234e6a7df7b44efe595fa6ef153f99309/test.csv', 'w', 1) as file:
        writer = csv.writer(file)
        writer.writerow(['列1', '列2'])
        writer.writerows(data)


if __name__ == "__main__":
    post_yun_zhijia()