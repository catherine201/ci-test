import React, { useEffect, useState } from 'react'
import { Input, Icon, Button, Avatar, Dropdown, Menu } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './header.module.less'
import { getParams } from '@/utils/index.js'

const { Search } = Input
const { Item } = Menu
const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_795441_oxvrei4jnr.js'
})

const carousels = [1, 2, 3, 4, 5, 6]

const headerMenu = [
  {
    path: '/',
    key: '',
    chineseName: '首页',
    englishName: 'Home'
  },
  {
    path: '/aboutLife',
    key: 'aboutLife',
    chineseName: '生活笔记',
    englishName: 'About Life'
  },
  {
    path: '/aboutTech',
    key: 'aboutTech',
    chineseName: '技术杂谈',
    englishName: 'About Tech'
  },
  {
    path: '/aboutMe',
    key: 'aboutMe',
    chineseName: '关于博主',
    englishName: 'About Me'
  },
  {
    path: '/leaveMsg',
    key: 'leaveMsg',
    chineseName: '给我留言',
    englishName: 'Leave Msg'
  },
  {
    path: '/Sponsor',
    key: 'Sponsor',
    chineseName: '赞助作者',
    englishName: 'Sponsor'
  }
]

const fixedMenu = [
  {
    name: 'qq',
    icon: 'qq',
    imagePath: require('../assets/images/qq.jpg')
  },
  {
    name: 'wc',
    icon: 'wechat',
    imagePath: require('../assets/images/wechat.jpg')
  },
  {
    name: 'wb',
    icon: 'weibo-circle',
    imagePath: require('../assets/images/wb.jpg'),
    link: 'https://weibo.com/2907751065/profile?topnav=1&wvr=6&is_all=1'
  },
  {
    name: 'jj',
    iconFont: 'hd-icon-index-juejin',
    imagePath: require('../assets/images/wb.jpg'),
    link: 'https://juejin.im/user/5c03296fe51d451aa843c033'
  }
]

const Header = (props: any) => {
  const [avatorUrl, setavatorUrl] = useState('') // 头像
  const [word, setword] = useState('') // 搜索
  const [remark, setremark] = useState('') // 地址
  const {
    userInfo,
    getPublishedFile,
    setSearchWordAction,
    searchWord,
    getPersonPublishedFile,
    location,
    articleFolder,
    getArticleFolder
  } = props
  const { pathname } = location
  console.log(pathname, 'pathname')
  useEffect(() => {
    const init = () => {
      const arr = pathname.split('/')
      setremark(arr[1])
      if (userInfo && userInfo.avator) {
        setavatorUrl(userInfo.avator)
      }
      setword(searchWord)
      if (!articleFolder.init) {
        getArticleFolder()
      }
    }
    init()
  }, [userInfo, searchWord, pathname, articleFolder, getArticleFolder])
  const toHref = (val: string) => {
    props.history.push(val)
  }

  const toWrite = () => {
    const isLogin = props.isLogin
    if (!isLogin) {
      props.history.push('/login?flag=write')
      return
    }
    if (!props.articleFolder.init) {
      props.getArticleFolder().then((res: { data: { page: { total: any }; datas: { _id: any }[] } }) => {
        if (res && res.data && res.data.page && res.data.page.total) {
          const obj = {
            pathId: res.data.datas[0]._id
          }
          props.getArticleFile(obj).then((result: { data: { datas: { _id: any }[] } }) => {
            if (result && result.data && result.data.datas && result.data.datas.length) {
              props.getArticleFileById(result.data.datas[0]._id).then(() => {
                props.history.push(`/article/notebooks/${res.data.datas[0]._id}/notes/${result.data.datas[0]._id}`)
              })
            } else {
              props.clearFileContent().then(() => {
                props.history.push(`/article/notebooks/${res.data.datas[0]._id}/notes`)
              })
            }
          })
        } else {
          props.history.push(`/article/notebooks`)
        }
      })
    } else {
      if (
        props.articleFolder &&
        props.articleFolder.data &&
        props.articleFolder.data.page &&
        props.articleFolder.data.page.total
      ) {
        const obj = {
          pathId: props.articleFolder.data.datas[0]._id
        }
        props.getArticleFile(obj).then((result: { data: { datas: { _id: any }[] } }) => {
          if (result && result.data && result.data.datas && result.data.datas.length) {
            props.getArticleFileById(result.data.datas[0]._id).then(() => {
              props.history.push(
                `/article/notebooks/${props.articleFolder.data.datas[0]._id}/notes/${result.data.datas[0]._id}`
              )
            })
          } else {
            props.clearFileContent().then(() => {
              props.history.push(`/article/notebooks/${props.articleFolder.data.datas[0]._id}/notes`)
            })
          }
        })
      } else {
        props.history.push(`/article/notebooks`)
      }
    }
  }
  // 退出登录
  const logOut = () => {
    props.setIsLoginAction(false)
    props.setUserInfoAction({})
  }

  const handleSearch = (value: any) => {
    console.log(props.match.params.openid)
    setSearchWordAction(value).then(() => {
      if (props.match.params.openid) {
        const obj = {
          openid: props.match.params.openid
        }
        props.resetPagination()
        getPersonPublishedFile(obj)
      } else {
        props.resetPagination()
        getPublishedFile()
      }
    })
  }

  const generateSubMenu = (index, value) => {
    let items: any = []
    let menus = []
    if (index === 1) {
      menus = articleFolder.data.datas.slice(0, 3)
    }
    if (index === 2) {
      menus = articleFolder.data.datas.slice(3)
    }
    // console.log(menus)
    // console.log(props.location.pathname + menu.pathName)
    items = menus.map(
      // tslint:disable-next-line: no-shadowed-variable
      (menu: any, index) => (
        <li
          key={menu._id}
          className={
            decodeURI(getParams('sub')) === menu.pathName
              ? `${styles.sub_menu_li} ${styles.sub_menu_li_active}`
              : `${styles.sub_menu_li}`
          }
          onClick={e => {
            // console.log(decodeURI(getParams('sub')), menu.pathName, getParams('sub') === menu.pathName)
            e.stopPropagation()
            // props.resetPagination()
            // setremark(menu.key)
            console.log(menu)
            const obj = {
              status: 1,
              pathId: menu._id
            }
            props.getArticleFile(obj).then(() => {
              props.history.push(`${value.path}?sub=${menu.pathName}&id=${menu._id}`)
            })
          }}
        >
          {menu.pathName}
        </li>
      )
    )
    return items
    // return ''
  }

  const generateMenu = (menus: any) => {
    // console.log()
    let items = []
    items = menus.map(
      // tslint:disable-next-line: no-shadowed-variable
      (menu, index) => (
        <li
          key={menu.key}
          className={`${styles.header_menu_li} ${menu.key === remark ? styles.header_menu_li_active : styles.header_menu_li
            }`}
          onClick={() => {
            props.resetPagination()
            setremark(menu.key)
            const arr =
              index === 1
                ? articleFolder.data.datas
                  .map(val => {
                    console.log(val)
                    return val._id
                  })
                  .slice(0, 3)
                : articleFolder.data.datas
                  .map(val => {
                    return val._id
                  })
                  .slice(3)
            const obj = {
              status: 1,
              pathId: arr
            }
            if (index === 1 || index === 2) {
              props.getArticleFile(obj).then(() => {
                props.history.push(menu.path)
              })
            } else if (index === 0) {
              props.getPublishedFile().then(() => {
                props.history.push(menu.path)
              })
            } else {
              props.history.push(menu.path)
            }
          }}
        >
          <span className={styles.a}>
            <i className={styles.chineseName}>{menu.chineseName}</i>
            <span className={styles.englishName}>{menu.englishName}</span>
          </span>
          {(index === 1 || index === 2) && articleFolder.init && (
            <ul className={styles.sub_menu_ul}>{generateSubMenu(index, menu)}</ul>
          )}
        </li>
      )
    )
    return items
  }

  const openWindow = link => {
    if (link) {
      window.open(link)
    }
  }

  const generateSide = (menus: any) => {
    let items = []
    // tslint:disable-next-line: no-shadowed-variable
    items = menus.map(menu => (
      <div
        className={`${styles.common_div}`}
        key={menu.name}
        onClick={() => {
          openWindow(menu.link)
        }}
      >
        {menu.icon ? (
          <Icon type={menu.icon} style={{ fontSize: '28px', color: '#666464' }} />
        ) : (
          <IconFont type={menu.iconFont} className={styles.icon} style={{ fontSize: '28px', color: '#666464' }} />
        )}
        <i className={styles.common_i} style={{ backgroundImage: `url(${menu.imagePath})` }} />
      </div>
    ))
    return items
  }

  const menu = (
    <Menu>
      <Item
        onClick={() => {
          toHref(`/art/personalArt/${props.userInfo.openid}/${props.userInfo.name}`)
        }}
      >
        <Icon type="user" style={{ marginRight: '5px' }} />
        我的主页
      </Item>
      {!(global as any).isMobile && (
        <Item
          onClick={() => {
            toHref(`/setting/basic`)
          }}
        >
          <Icon type="setting" style={{ marginRight: '5px' }} />
          设置
        </Item>
      )}
      {(props.userInfo.type === 'admin' || props.userInfo.type === 'super') && (
        <Item
          onClick={() => {
            toHref(`/verify/article?status=4`)
          }}
        >
          <Icon type="check-circle" style={{ marginRight: '5px' }} />
          文章审核
        </Item>
      )}
      <Item
        onClick={() => {
          logOut()
        }}
      >
        <Icon type="logout" style={{ marginRight: '5px' }} />
        退出
      </Item>
    </Menu>
  )

  const generateImg = (menus: any) => {
    let items = []
    items = menus.map((menu: any, index: number) => (
      <img src={require(`@/assets/images/web${index + 1}.jpg`)} alt="" className={styles.carouselImg} key={index} />
    ))
    return items
  }

  return (
    <div className={styles.home_top_wrap}>
      <div className={styles.home_top_title}>
        <div className={`home_top ${styles.home_top}`}>
          <div className={styles.logo}>
            <h1>
              <a href="/" title="欢迎来到Catherine的网站">
                欢迎来到Catherine的网站
              </a>
            </h1>
          </div>
          <ul className={styles.header_menu}>{generateMenu(headerMenu)}</ul>
        </div>
      </div>
      <div className={styles.welcom_wrap}>
        <div className={styles.welcom_left}>
          <Icon type="sound" style={{ fontSize: '18px', color: '#56af45' }} className={styles.sound} />
          <span className={styles.ani}>欢迎来访～</span>
        </div>
        <Search
          placeholder="insert"
          onSearch={value => handleSearch(value)}
          style={{ width: 200 }}
          value={word}
          onChange={e => {
            setword(e.target.value)
            setSearchWordAction(e.target.value)
          }}
          allowClear={true}
        />
        <div>
          <div className={styles.right_top}>
            {props.isLogin ? (
              <Dropdown overlay={menu} placement="bottomCenter">
                {avatorUrl ? (
                  <img src={avatorUrl} className={styles.Avatar} alt="头像" />
                ) : (
                  <Avatar size={40} icon="user" className={styles.Avatar} />
                )}
              </Dropdown>
            ) : (
              <div>
                <span
                  className={styles.login_btn}
                  onClick={() => {
                    toHref('/login')
                  }}
                >
                  登录
                </span>
                {/* {!(global as any).isMobile && (
                  <Button
                    className={styles.btn}
                    onClick={() => {
                      toHref('/register')
                    }}
                  >
                    注册
                  </Button>
                )} */}
              </div>
            )}

            {!(global as any).isMobile && (
              <Button
                type="primary"
                icon="edit"
                className={styles.btn}
                onClick={() => {
                  // toHref('/article/notebooks')
                  toWrite()
                }}
              >
                写文章
              </Button>
            )}
          </div>
        </div>
      </div>
      <div className={styles.fx}>{generateSide(fixedMenu)}</div>
      {!(global as any).isMobile && (window as any).innerWidth - 0 > 1500 && (
        <div className={styles.rote_wrap}>{generateImg(carousels)}</div>
      )}
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  articleFile: state.article.articleFile,
  articleFolder: state.article.articleFolder,
  isLogin: state.account.isLogin,
  userInfo: state.account.userInfo,
  searchWord: state.article.searchWord
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFile: dispatch.article.getArticleFile,
  getArticleFolder: dispatch.article.getArticleFolder, // 获取文集列表
  getArticleFileById: dispatch.article.getArticleFileById,
  clearFileContent: dispatch.article.clearFileContent,
  getPublishedFile: dispatch.article.getPublishedFile,
  setIsLoginAction: dispatch.account.setIsLoginAction,
  setUserInfoAction: dispatch.account.setUserInfoAction,
  setSearchWordAction: dispatch.article.setSearchWordAction,
  getPersonPublishedFile: dispatch.article.getPersonPublishedFile,
  resetPagination: dispatch.searchOption.resetPagination
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Header)
)
