import React from 'react'
import { Menu, Icon } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './settingWrap.module.less'

const SideBar = (props: any) => {
  const menus = [
    {
      key: 'basic',
      path: '/setting/basic',
      text: '基础设置',
      icon: 'menu'
    },
    {
      key: 'changePsw',
      path: '/setting/changePsw',
      text: '更改密码',
      icon: 'sync'
    }
  ]

  if (props.userInfo.type === 'admin' || props.userInfo.type === 'super') {
    menus.push({
      key: 'user',
      path: '/setting/user',
      text: '用户管理',
      icon: 'user'
    })
  }

  const toHref = path => {
    props.history.push(path)
  }
  const generateMenu = (values: any) => {
    let items: any = []
    const arr = props.location.pathname.split('/')
    items = values.map(menu => (
      <Menu.Item
        key={menu.key}
        className={arr[arr.length - 1] === menu.key ? styles['list-item-active'] : styles['list-item']}
        onClick={() => {
          props.resetPagination()
          toHref(menu.path)
        }}
      >
        <Icon type={menu.icon} style={{ marginRight: '5px' }} />
        <span className="nav-text">{menu.text}</span>
      </Menu.Item>
    ))
    return items
  }
  console.log('sideBar223')
  return (
    <div className={styles.side_bar_wrap}>
      <Menu>{generateMenu(menus)}</Menu>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  articleFile: state.article.articleFile,
  userInfo: state.account.userInfo
})

const mapDispatchToProps = (dispatch: any) => ({
  resetPagination: dispatch.searchOption.resetPagination
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SideBar)
)
