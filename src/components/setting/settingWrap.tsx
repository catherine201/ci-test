import React from 'react'
import { withRouter } from 'react-router-dom'
import WrappedRoute from '../WrapRoute.js'
import { connect } from 'react-redux'
import styles from './settingWrap.module.less'
import Header from '@/components/header.tsx'
import SideBar from './sideBar.tsx'
const Setting = (props: any) => {
  return (
    <div className={styles.setting_wrap}>
      {/* <Header /> */}
      {!(global as any).isMobile && <Header />}
      <div className={styles.content}>
        {/* <SideBar /> */}
        {!(global as any).isMobile && <SideBar />}
        {props.children}
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  articleFile: state.article.articleFile
})

const mapDispatchToProps = (dispatch: any) => ({
  clearFileContent: dispatch.article.clearFileContent
})

export default WrappedRoute(
  withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(Setting)
  )
)
