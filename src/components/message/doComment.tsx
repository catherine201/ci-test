import React, { useEffect, useState } from 'react'
import styles from './index.module.less'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Input, Button, Avatar, message } from 'antd'

const { TextArea } = Input
const DoComment = props => {
  const [word, setword] = useState('') // 搜索
  const ref = React.createRef()
  useEffect(() => {
    if (props.pid) {
      // tslint:disable-next-line: whitespace
      console.log((ref as any).current.textAreaRef)
      if ((ref as any).current.textAreaRef && (ref as any).current.textAreaRef.offsetTop) {
        window.scrollTo({
          top: (ref as any).current.textAreaRef.offsetTop - 400
        })
      }
      // tslint:disable-next-line: whitespace
      ;(ref as any).current.focus()
    }
    // console.log((ref as any).current.textAreaRef)
  }, [props, ref])

  const submitComment = () => {
    if (!props.isLogin) {
      message.warning('请先登录')
      props.history.push('/login')
      return
    }
    const obj = {
      comment: word
    }
    if (props.pid) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).pid = props.pid
    }
    if (props.location.pathname.split('/')[1]) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).type = props.location.pathname.split('/')[1]
    }
    if (props.match.params.id) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).articleId = props.match.params.id
    }
    props.createComment(obj).then(res => {
      setword('')
      props.finishSubmit(JSON.parse(JSON.stringify(res)))
    })
  }

  return (
    <div className={props.pid ? `${styles.do_comment_wrap} ${styles.border}` : styles.do_comment_wrap}>
      <div className={styles.owner_title}>
        {props.userInfo && props.userInfo.avator ? (
          <img src={props.userInfo.avator} className={styles.Avatar} alt="头像" />
        ) : (
          <Avatar size={40} icon="user" className={styles.Avatar} />
        )}
        <span className={styles.name}>{props.userInfo && props.userInfo.name ? props.userInfo.name : ''}</span>
        发表我的评论
        {!props.noCancel ? (
          <div
            className={styles.cancel_btn}
            onClick={() => {
              props.cancelComment()
            }}
          >
            取消评论
          </div>
        ) : (
          ''
        )}
      </div>
      <div className={styles.write_wrap}>
        <TextArea
          placeholder="写点什么……"
          ref={ref as any}
          autosize={true}
          value={word}
          onChange={e => {
            setword(e.target.value)
          }}
          onPressEnter={() => {
            submitComment()
          }}
        />
        <Button
          className={styles.btn}
          type="primary"
          icon="check-square"
          onClick={() => {
            submitComment()
          }}
        >
          提交评论
        </Button>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  isLogin: state.account.isLogin,
  userInfo: state.account.userInfo,
  commentList: state.comment.commentList
})

const mapDispatchToProps = (dispatch: any) => ({
  createComment: dispatch.comment.createComment
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DoComment)
)
