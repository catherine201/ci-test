import React, { useEffect, useState } from 'react'
import styles from './index.module.less'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Avatar, Icon, Pagination } from 'antd'
import DoComment from './doComment'
import { handleComment } from '@/utils/index.js'

const LIMIT = 10
const Message = props => {
  const [clickInfo, setclickInfo] = useState([]) // 点击状态
  const [pid, setpid] = useState([]) // pid
  const [remark, setremark] = useState('') // location
  const { getComments, pagination, getPagination, location, match } = props
  const { pathname, hash } = location
  console.log(patchname, '321421')
  console.log(LIMIT, 'LIMITtest1268')
  useEffect(() => {
    const arr = pathname.split('/')
    setremark(arr[1])
    const obj = { type: arr[1] }
    if (match.params.id) {
      // tslint:disable-next-line: whitespace
      ; (obj as any).articleId = match.params.id
    }
    if (hash) {
      // tslint:disable-next-line: whitespace
      ; (document.querySelector('#comments') as any).scrollIntoView({
        block: 'start',
        behavior: 'smooth'
      })
    }
    // 给我留言  赞助  关于博主 和文章绑定的评论
    getComments(obj).then(res => {
      setclickInfo(handleComment([...res.data.datas])) // 处理点击
    })
  }, [getComments, match, hash, pathname])

  const handleAnswer = menu => {
    setpid(menu._id)
    const copy = JSON.parse(JSON.stringify(clickInfo))
    const result = copy.map(res => {
      return {
        id: res.id,
        click: res.id === menu._id ? true : false
      }
    })
    setclickInfo(result)
  }

  const checkShow = menu => {
    const res = clickInfo.filter((item: any) => {
      return item.id === menu._id
    })[0]
    return res && (res as any).click
  }

  const finishSubmit = value => {
    cancelComment()
    setclickInfo(handleComment([...value.data.datas])) // 处理点击
  }

  const handleObj = menu => {
    return menu.children ? (
      <li key={menu._id}>
        <div className={styles.list_li}>
          {menu.account && menu.account.length && menu.account[0].avator ? (
            <img src={menu.account[0].avator} className={styles.Avatar} alt="头像" />
          ) : (
            <Avatar size={40} icon="user" className={styles.Avatar} />
          )}
          <div className={styles.comment_right}>
            <div> {menu.comment}</div>
            <div>
              <span className={styles.name}>{menu.account[0].name}</span>
              <span className={styles.date}>
                {menu.account[0].updated_at &&
                  (new Date(menu.account[0].updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
              </span>
              <span
                className={styles.answer_btn}
                // tslint:disable-next-line: no-shadowed-variable
                onClick={() => {
                  console.log(remark)
                  handleAnswer(menu)
                }}
              >
                回复
              </span>
              {props.userInfo.type === 'super' ? (
                <span
                  className={styles.delete_btn}
                  onClick={() => {
                    const obj = { id: menu._id, type: remark }
                    if (match.params.id) {
                      // tslint:disable-next-line: whitespace
                      ; (obj as any).articleId = match.params.id
                    }
                    props.deleteComment(obj)
                  }}
                >
                  删除
                </span>
              ) : (
                ''
              )}
            </div>
          </div>
        </div>
        {checkShow(menu) ? (
          //   <TransitionGroup>
          //     <CSSTransition classNames="fade" timeout={500}>
          <DoComment pid={pid} finishSubmit={finishSubmit} cancelComment={cancelComment} />
        ) : (
          //     </CSSTransition>
          //   </TransitionGroup>
          ''
        )}
        <ul>{handleChildren(menu.children)}</ul>
      </li>
    ) : (
      <React.Fragment key={menu._id}>
        <li key={menu._id}>
          <div className={styles.list_li}>
            {menu.account && menu.account.length && menu.account[0].avator ? (
              <img src={menu.account[0].avator} className={styles.Avatar} alt="头像" />
            ) : (
              <Avatar size={40} icon="user" className={styles.Avatar} />
            )}
            <div className={styles.comment_right}>
              <div> {menu.comment}</div>
              <div>
                <span className={styles.name}>{menu.account[0].name}</span>
                <span className={styles.date}>
                  {menu.account[0].updated_at &&
                    (new Date(menu.account[0].updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
                </span>
                <span
                  className={styles.answer_btn}
                  // tslint:disable-next-line: no-shadowed-variable
                  onClick={() => {
                    console.log(remark)
                    handleAnswer(menu)
                  }}
                >
                  回复
                </span>
                {props.userInfo.type === 'super' ? (
                  <span
                    className={styles.delete_btn}
                    onClick={() => {
                      const obj = { id: menu._id, type: remark }
                      if (match.params.id) {
                        // tslint:disable-next-line: whitespace
                        ; (obj as any).articleId = match.params.id
                      }
                      props.deleteComment(obj)
                    }}
                  >
                    删除
                  </span>
                ) : (
                  ''
                )}
              </div>
            </div>
          </div>
        </li>
        {checkShow(menu) ? (
          //   <TransitionGroup>
          //     <CSSTransition classNames="fade" timeout={500}>
          <DoComment pid={pid} finishSubmit={finishSubmit} cancelComment={cancelComment} />
        ) : (
          //     </CSSTransition>
          //   </TransitionGroup>
          ''
        )}
      </React.Fragment>
    )
  }

  const cancelComment = () => {
    const copy = JSON.parse(JSON.stringify(clickInfo))
    const result = copy.map(res => {
      return {
        id: res.id,
        click: false
      }
    })
    setclickInfo(result)
  }

  const handleChildren = arr => {
    let items = []
    items = arr.map(item => {
      return item.children ? (
        <li key={item._id}>
          <div className={styles.list_li}>
            {item.account && item.account.length && item.account[0].avator ? (
              <img src={item.account[0].avator} className={styles.Avatar} alt="头像" />
            ) : (
              <Avatar size={40} icon="user" className={styles.Avatar} />
            )}
            <div className={styles.comment_right}>
              <div> {item.comment}</div>
              <div>
                <span className={styles.name}>{item.account[0].name}</span>
                <span className={styles.date}>
                  {item.account[0].updated_at &&
                    (new Date(item.account[0].updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
                </span>
                <span
                  className={styles.answer_btn}
                  // tslint:disable-next-line: no-shadowed-variable
                  onClick={() => {
                    console.log(remark)
                    handleAnswer(item)
                  }}
                >
                  回复
                </span>
                {props.userInfo.type === 'super' ? (
                  <span
                    className={styles.delete_btn}
                    onClick={() => {
                      const obj = { id: item._id, type: remark }
                      if (match.params.id) {
                        // tslint:disable-next-line: whitespace
                        ; (obj as any).articleId = match.params.id
                      }
                      props.deleteComment(obj)
                    }}
                  >
                    删除
                  </span>
                ) : (
                  ''
                )}
              </div>
            </div>
          </div>
          {checkShow(item) ? (
            // <TransitionGroup>
            //   <CSSTransition classNames="fade" timeout={500}>
            <DoComment pid={pid} finishSubmit={finishSubmit} cancelComment={cancelComment} />
          ) : (
            //   </CSSTransition>
            // </TransitionGroup>
            ''
          )}
          <ul>{handleChildren(item.children)}</ul>
        </li>
      ) : (
        <li key={item._id}>
          <div className={styles.list_li}>
            {item.account && item.account.length && item.account[0].avator ? (
              <img src={item.account[0].avator} className={styles.Avatar} alt="头像" />
            ) : (
              <Avatar size={40} icon="user" className={styles.Avatar} />
            )}
            <div className={styles.comment_right}>
              <div> {item.comment}</div>
              <div>
                <span className={styles.name}>{item.account[0].name}</span>
                <span className={styles.date}>
                  {item.account[0].updated_at &&
                    (new Date(item.account[0].updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
                </span>
                <span
                  className={styles.answer_btn}
                  // tslint:disable-next-line: no-shadowed-variable
                  onClick={() => {
                    console.log(remark)
                    handleAnswer(item)
                  }}
                >
                  回复
                </span>
                {props.userInfo.type === 'super' ? (
                  <span
                    className={styles.delete_btn}
                    onClick={() => {
                      const obj = { id: item._id, type: remark }
                      if (match.params.id) {
                        // tslint:disable-next-line: whitespace
                        ; (obj as any).articleId = match.params.id
                      }
                      props.deleteComment(obj)
                    }}
                  >
                    删除
                  </span>
                ) : (
                  ''
                )}
              </div>
            </div>
          </div>
          {checkShow(item) ? (
            // <TransitionGroup>
            //   <CSSTransition classNames="fade" timeout={500}>
            <DoComment pid={pid} finishSubmit={finishSubmit} cancelComment={cancelComment} />
          ) : (
            //   </CSSTransition>
            // </TransitionGroup>
            ''
          )}
        </li>
      )
    })
    return items
  }

  const generateComment = (menus: any) => {
    console.log(menus)
    let items = []
    items = menus.map(menu => {
      return handleObj(menu)
    })
    return items
  }

  // 翻页
  const handleTableChange = page => {
    console.log(page)
    const pager = { ...pagination }
    pager.current = page
    const obj = {
      limit: LIMIT,
      offset: (page - 1) * LIMIT
    }
    if (remark) {
      // tslint:disable-next-line: whitespace
      ; (obj as any).remark = remark
    }
    if (match.params.id) {
      // tslint:disable-next-line: whitespace
      ; (obj as any).articleId = match.params.id
    }
    getComments(obj).then(res => {
      setclickInfo(handleComment([...res.data.datas])) // 处理点击
    })
    getPagination(pager)
  }

  return (
    <div className={styles.message_wrap} id="comments">
      <div className={styles.message_content}>
        <DoComment noCancel={true} remark={remark} finishSubmit={finishSubmit} />
        {props.commentList.init && (
          <div className={styles.comment_list_wrap}>
            {props.commentList.data.count > 0 && (
              <p className={styles.total_count}>
                <Icon type="message" /> ({props.commentList.data.count})个小伙伴在吐槽
              </p>
            )}
            <ul className={styles.comment_list_ul}>{generateComment(props.commentList.data.datas)}</ul>
            <Pagination
              {...pagination}
              // tslint:disable-next-line: no-shadowed-variable
              onChange={pagination => {
                handleTableChange(pagination)
              }}
              hideOnSinglePage={true}
              className={styles.pagination}
            />
          </div>
        )}
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  userInfo: state.account.userInfo,
  commentList: state.comment.commentList,
  pagination: state.searchOption.pagination
})

const mapDispatchToProps = (dispatch: any) => ({
  createComment: dispatch.comment.createComment,
  getComments: dispatch.comment.getComments,
  deleteComment: dispatch.comment.deleteComment,
  getPagination: dispatch.searchOption.getPagination
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Message)
)
