import React, { useEffect, useState } from 'react'
import { Icon, Input } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './component.module.less'

const { Search } = Input
const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_795441_cmez8lrpqam.js'
})
const bottomMenu = [
  {
    name: '首页',
    icon: 'home',
    link: '/',
    key: ''
  },
  {
    name: '生活',
    iconFont: 'hd-icon-index-shenghuo-copy',
    link: '/aboutLife',
    key: 'aboutLife'
  },
  {
    name: '技术',
    iconFont: 'hd-icon-index-jishu',
    link: '/aboutTech',
    key: 'aboutTech'
  },
  {
    name: '我',
    icon: 'user',
    link: '/me',
    key: 'me'
  }
]

const Bottom = (props: any) => {
  const [word, setword] = useState('') // 搜索
  const [remark, setremark] = useState('') // 地址
  const {
    getPublishedFile,
    setSearchWordAction,
    getPersonPublishedFile,
    location,
    searchWord,
    articleFolder,
    getArticleFolder
  } = props
  const { pathname } = location
  console.log(pathname)
  useEffect(() => {
    const init = () => {
      const arr = pathname.split('/')
      setremark(arr[1])
      setword(searchWord)
      if (!articleFolder.init) {
        getArticleFolder()
      }
    }
    init()
  }, [searchWord, pathname, articleFolder, getArticleFolder])
  const generateBottom = (menus: any) => {
    let items = []
    // tslint:disable-next-line: no-shadowed-variable
    items = menus.map((menu, index) => (
      <div
        className={`${
          styles.bottom_item // {`${styles.bottom_item}`}
        } ${menu.key === remark ? styles.bottom_item_active : styles.bottom_item}`}
        key={menu.name}
        onClick={() => {
          // openWindow(menu.link)
          props.resetPagination()
          setremark(menu.key)
          const arr =
            index === 1
              ? articleFolder.data.datas
                  .map(val => {
                    console.log(val)
                    return val._id
                  })
                  .slice(0, 3)
              : articleFolder.data.datas
                  .map(val => {
                    return val._id
                  })
                  .slice(3)
          const obj = {
            status: 1,
            pathId: arr
          }
          if (index === 1 || index === 2) {
            props.getArticleFile(obj).then(() => {
              props.history.push(menu.link)
            })
          } else if (index === 0) {
            props.getPublishedFile().then(() => {
              props.history.push(menu.link)
            })
          } else {
            props.history.push(menu.link)
          }
          //   props.history.push(menu.link)
        }}
      >
        {menu.icon ? (
          <Icon type={menu.icon} className={styles.icon} style={{ fontSize: '20px' }} />
        ) : (
          <IconFont type={menu.iconFont} className={styles.icon} style={{ fontSize: '25px' }} />
        )}
        <span>{menu.name}</span>
      </div>
    ))
    return items
  }

  const handleSearch = (value: any) => {
    console.log(props.match.params.openid)
    setSearchWordAction(value).then(() => {
      if (props.match.params.openid) {
        const obj = {
          openid: props.match.params.openid
        }
        props.resetPagination()
        getPersonPublishedFile(obj)
      } else {
        props.resetPagination()
        getPublishedFile()
      }
    })
  }

  return (
    <div className={styles.outer_wrap}>
      {pathname !== '/me' && (
        <div className={props.back ? `${styles.back_top_search} ${styles.top_search}` : styles.top_search}>
          {props.back && (
            <Icon
              type="left"
              style={{ fontSize: '16px', color: '#007fff' }}
              className="icon_back"
              onClick={() => {
                console.log('click')
                props.history.go(-1)
              }}
            />
          )}
          <Search
            placeholder="搜索文章"
            onSearch={value => handleSearch(value)}
            className={styles.search_input}
            value={word}
            onChange={e => {
              setword(e.target.value)
              setSearchWordAction(e.target.value)
            }}
            allowClear={true}
          />
        </div>
      )}
      <div className={styles.bottom_wrap}>{generateBottom(bottomMenu)}</div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  searchWord: state.article.searchWord,
  articleFolder: state.article.articleFolder
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFile: dispatch.article.getArticleFile,
  getArticleFolder: dispatch.article.getArticleFolder, // 获取文集列表
  getPublishedFile: dispatch.article.getPublishedFile,
  setSearchWordAction: dispatch.article.setSearchWordAction,
  getPersonPublishedFile: dispatch.article.getPersonPublishedFile,
  resetPagination: dispatch.searchOption.resetPagination
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Bottom)
)
