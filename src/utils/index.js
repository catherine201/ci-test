/* eslint-disable no-useless-escape */
/* eslint-disable no-extend-native */
export function init () {
  // Warn if overriding existing method
  if (Array.prototype.equals) {
    console.warn(
      "Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code."
    )
  }
  // attach the .equals method to Array's prototype to call it on any array
  Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array) return false
    // compare lengths - can save a lot of time
    if (this.length !== array.length) return false
    for (let i = 0, l = this.length; i < l; i++) {
      // Check if we have nested arrays
      if (this[i] instanceof Array && array[i] instanceof Array) {
        // recurse into the nested arrays
        if (!this[i].equals(array[i])) return false
      } else if (this[i] !== array[i]) {
        // Warning - two different object instances will never be equal: {x:20} != {x:20}
        return false
      }
    }
    return true
  }
  String.prototype.endWith = function (endStr) {
    const d = this.length - endStr.length
    return d >= 0 && this.lastIndexOf(endStr) === d
  }
  // Hide method from for-in loops
  Object.defineProperty(Array.prototype, 'equals', { enumerable: false })

  Date.prototype.format = function (fmt) {
    const o = {
      'M+': this.getMonth() + 1, // 月份
      'd+': this.getDate(), // 日
      'h+': this.getHours(), // 小时
      'm+': this.getMinutes(), // 分
      's+': this.getSeconds(), // 秒
      'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
      S: this.getMilliseconds() // 毫秒
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, `${this.getFullYear()}`.substr(4 - RegExp.$1.length))
    }
    for (const k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : `00${o[k]}`.substr(`${o[k]}`.length))
      }
    }
    return fmt
  }
  var ua = navigator.userAgent

  var ipad = ua.match(/(iPad).*OS\s([\d_]+)/)
  var isIphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/)
  var isAndroid = ua.match(/(Android)\s+([\d.]+)/)
  var isMobile = isIphone || isAndroid
  global.isMobile = isMobile
}

export function getParams (key) {
  if (window.location.href.indexOf('?') !== -1) {
    const str = window.location.href.split('?')[1]
    if (str.indexOf('&') !== -1) {
      const arr = str.split('&')
      let output = ''
      arr.forEach(item => {
        const ar = item.split('=')
        if (ar[0] === key) {
          output = ar[1]
        }
      })
      return output
    }
    const arr = str.split('=')
    if (key === arr[0]) {
      return arr[1]
    }
  }
}

const getResult = function (list) {
  list.forEach(function (item) {
    if (item.childRoutes && item.childRoutes.length) {
      // eslint-disable-next-line array-callback-return
      item.childRoutes.map((it, ind) => {
        it.path = item.path + it.path
        if (it.childRoutes && it.childRoutes.length) {
          getResult(item.childRoutes)
        }
      })
    }
  })
}

export function handleRoute (routeConfig) {
  var copy = routeConfig
  getResult(routeConfig)
  return copy
}

// 获取路由列表与现路由的匹配
let routeObj = {}
export const getRouteObj = function (config, path) {
  config.forEach(item => {
    let path1 = ''
    const arr = item.path.split('/')
    arr.forEach((it, ind) => {
      if (it.startsWith(':')) {
        // delete arr[ind]
        arr.length = ind
      }
    })
    path1 = arr.join('/')
    if (path1 === path) {
      routeObj = item
    } else if (item.childRoutes && item.childRoutes.length) {
      getRouteObj(item.childRoutes, path)
    }
  })
  return routeObj
}

export function getNowFormatDate () {
  const date = new Date()
  const seperator1 = '-'
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  let strDate = date.getDate()
  if (month >= 1 && month <= 9) {
    month = `0${month}`
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = `0${strDate}`
  }
  const currentdate = year + seperator1 + month + seperator1 + strDate
  return currentdate
}

export function FirstLastChange (arr) {
  const last = arr.pop()
  arr.unshift(last)
  return JSON.parse(JSON.stringify(arr))
}

export function wrapRed (str, word) {
  return str.split(word).join('<span style="color: red">' + word + '</span>')
}

export function handleAbstract (content, word, add = 100) {
  if (content) {
    const newContent = content.replace(/<[\/\!]*[^<>]*>/gi, '')
    const index = newContent.indexOf(word)
    let newStr = ''
    if (index !== -1) {
      newStr = newContent.slice(index > 10 ? index - 10 : 0, index + add)
    } else {
      newStr = newContent.slice(0, add)
    }
    return index !== -1 && index !== 0
      ? `......${newStr.split(word).join('<span style="color: red">' + word + '</span>')}......`
      : `${newStr.split(word).join('<span style="color: red">' + word + '</span>')}......`
  } else {
    return ''
  }
}
const handleCommentObj = function (items, menu) {
  items.push({ id: menu._id, click: false })
  if (menu.children) {
    menu.children.forEach(res => {
      handleCommentObj(items, res)
    })
  }
}

export function handleComment (menus) {
  const items = []
  menus.forEach(menu => {
    handleCommentObj(items, menu)
  })
  console.log(items)
  return items
}

export function getDateDiff (dateTimeStamp) {
  let result = ''
  var minute = 1000 * 60
  var hour = minute * 60
  var day = hour * 24
  // var halfamonth = day * 15
  var month = day * 30
  var now = new Date().getTime()
  var diffValue = now - new Date(dateTimeStamp).getTime()
  if (diffValue < 0) {
    return
  }
  var monthC = diffValue / month
  var weekC = diffValue / (7 * day)
  var dayC = diffValue / day
  var hourC = diffValue / hour
  var minC = diffValue / minute
  if (monthC >= 1) {
    result = '' + parseInt(monthC) + '月前'
  } else if (weekC >= 1) {
    result = '' + parseInt(weekC) + '周前'
  } else if (dayC >= 1) {
    result = '' + parseInt(dayC) + '天前'
  } else if (hourC >= 1) {
    result = '' + parseInt(hourC) + '小时前'
  } else if (minC >= 1) {
    result = '' + parseInt(minC) + '分钟前'
  } else result = '刚刚'
  return result
}

/**
 * 获取字符串中某个匹配字符串的个数
 * @param {*} scrstr 源字符串
 * @param {*} armstr 匹配字符
 * @param {*} ignoreI  是否忽略大小写，默认为忽略大小写
 */
export function getStrCount (scrstr, armstr, ignoreI = true) {
  const needEscapeChar = ['$', '(', ')', '*', '+', '.', '[', ']', '?', '^', '{', '}', '|']
  const result = scrstr.match(
    new RegExp(needEscapeChar.indexOf(armstr) > -1 ? `\\${armstr}` : armstr, `g${ignoreI ? 'i' : ''}`)
  )
  return result ? result.length : 0
}
