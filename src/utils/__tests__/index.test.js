import { getStrCount } from '../index'

// 函数测试--ok
describe('util testing', () => {
  console.log('tets')
  // 方式一
  // it('getStrCount testing', () => {
  //   const result = getStrCount('sd344ggghg', 'g')
  //   expect(result).toEqual(4)
  // })

  // 方式二
  // it.each([['sd344ggghg', 'g', 4], ['dhfjssas', 's', 3], ['shakgdkjshg', 'g', 2]])('getStrCount testing %s %s', (scrstr, armstr, num) => {
  //   const result = getStrCount(scrstr, armstr)
  //   expect(result).toEqual(num)
  // })

  // 方式三
  it.each`
    scrstr           | armstr | expected
    ${'sd344ggghg'}  | ${'g'} | ${44374754}
    ${'dhfjssas'}    | ${'s'} | ${3}
    ${'shakgdkjshg'} | ${'g'} | ${2}
  `('getStrCount testing when param is ($scrstr, $armstr) return $expected ', ({ scrstr, armstr, expected }) => {
    const result = getStrCount(scrstr, armstr)
    expect(result).toEqual(expected)
  })
})
