import React from 'react'
import Header from '@/components/header.tsx'
import Message from '@/components/message/index.tsx'
import styles from './index.module.less'

const MessageMe = () => {
  return (
    <div className={styles.message_wrap}>
      <Header />
      <div className={styles.message_content_wrap}>
        <div className="common_title">给我留言</div>
        <Message />
      </div>
    </div>
  )
}

export default MessageMe
