import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Tloader from 'react-touch-loader'
import styles from './index.module.less'
import constant from '@/utils/constant.js'
import Header from '@/components/header.tsx'
import Bottom from '@/components/bottom.tsx'
import { Icon } from 'antd'
import { wrapRed, handleAbstract, getDateDiff } from '@/utils/index.js'
import { getParams } from '@/utils/index.js'

const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_795441_mxj9e6qdfxq.js'
})
const add = (global as any).isMobile ? 30 : 100
console.log(add)
const PersonalArt = (props: any) => {
  const { articleFile, getArticleFile, match } = props
  const { pathId } = match.params

  useEffect(() => {
    // if (pathId) {
    //   const obj = {
    //     status: 1,
    //     pathId
    //   }
    //   getArticleFile(obj)
    // }
  }, [getArticleFile, pathId])

  const toDetail = (val: any) => {
    if ((global as any).isMobile) {
      props.history.push(`/art/articleContent/${val}`)
    } else {
      window.open(window.origin + `/#/art/articleContent/${val}`)
    }
  }

  const loadMore = () => {
    const arr =
      props.location.pathname === 'aboutLife'
        ? props.articleFolder.data.datas
            .map(val => {
              console.log(val)
              return val._id
            })
            .slice(0, 3)
        : props.articleFolder.data.datas
            .map(val => {
              return val._id
            })
            .slice(3)

    const obj = {
      status: 1,
      pathId: pathId || getParams('id') || arr,
      limit: constant.LIMIT,
      offset: articleFile.data.page.offset + articleFile.data.datas.length,
      add: 'add'
    }
    props.getArticleFile(obj)
  }

  const changeLike = (e, menu) => {
    const obj = {
      id: menu._id
    }
    if (menu.thumb_status) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).status = false
    }
    console.log(e.currentTarget.children[1])
    if (!menu.thumb_status) {
      e.currentTarget.children[1].className = 'i slideInUp'
    } else {
      e.currentTarget.children[1].className = 'i'
    }
    props.UpdateArticleThumbsUpNumById(obj).then(res => {
      console.log(res)
      if (res) {
        const arr =
          props.location.pathname === 'aboutLife'
            ? props.articleFolder.data.datas
                .map(val => {
                  console.log(val)
                  return val._id
                })
                .slice(0, 3)
            : props.articleFolder.data.datas
                .map(val => {
                  return val._id
                })
                .slice(3)
        const sendObj = {
          status: 1,
          pathId: pathId || getParams('id') || arr,
          limit: articleFile.data.datas.length,
          offset: 0
        }
        getArticleFile(sendObj)
      }
    })
  }

  const generateRecomand = (menus: any) => {
    let items = []
    items = menus.map(menu => (
      <div key={menu._id} className={styles.list_item}>
        <div className={styles.list_item_left}>
          {/* <p
            className={styles.title}
            onClick={() => {
              toDetail(menu._id)
            }}
            dangerouslySetInnerHTML={{ __html: wrapRed(menu.title, props.searchWord) as any }}
          /> */}
          <div className={styles.top_title}>
            {menu.paths && menu.paths[0] && (
              <div className={styles.path}>{menu.paths && menu.paths[0] && menu.paths[0].pathName}</div>
            )}
            <p
              className={styles.title}
              onClick={() => {
                toDetail(menu._id)
              }}
              dangerouslySetInnerHTML={{ __html: wrapRed(menu.title, props.searchWord) as any }}
            />
          </div>
          <div dangerouslySetInnerHTML={{ __html: handleAbstract(menu.content, props.searchWord, add) as any }} />
          <div className={styles.desc}>
            {/* {menu.paths && menu.paths[0] && (
              <span className={`${styles.author} ${styles.viewCount}`}>
                <Icon type="folder" className={styles.icon} />
                {menu.paths && menu.paths[0] && menu.paths[0].pathName}
              </span>
            )} */}
            <span className={`${styles.author} ${styles.viewCount}`}>
              <Icon type="user" className={styles.icon} />
              {menu.author}
            </span>
            <span>
              <IconFont
                type="hd-icon-index-shijian"
                className={styles.icon}
                style={{ fontSize: '16px', color: '#999' }}
              />
              {menu.updated_at &&
                !(global as any).isMobile &&
                (new Date(menu.updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
              {menu.updated_at && (global as any).isMobile && getDateDiff(menu.updated_at)}
            </span>
            <span>
              <Icon type="eye" className={styles.icon} />
              {menu.view_count}浏览
            </span>
            <span
              className={`${styles.viewCount} ${styles.thumbs}`}
              onClick={e => {
                changeLike(e, menu)
              }}
            >
              <Icon type="like" className={menu.thumb_status ? `${styles.icon} ${styles.active}` : `${styles.icon}`} />
              {menu.thumbsUp_count}
              <i className="i">+1</i>
            </span>
            <span
              className={styles.viewCount}
              onClick={() => {
                if ((global as any).isMobile) {
                  props.history.push(`/art/articleContent/${menu._id}#comments`)
                } else {
                  window.open(window.origin + `/#/art/articleContent/${menu._id}#comments`)
                }
              }}
            >
              <Icon type="message" className={styles.icon} style={{ color: '#1DA57A' }} />
              {menu.comment.length}
              {!(global as any).isMobile && '评论'}
            </span>
          </div>
        </div>
        {!(global as any).isMobile && (
          <div className={styles.img_wrap}>
            {menu.pictureUrl ? (
              <img
                src={menu.pictureUrl}
                alt=""
                className={styles.img}
                onClick={() => {
                  toDetail(menu._id)
                }}
              />
            ) : (
              ''
            )}
          </div>
        )}
      </div>
    ))
    return items
  }

  return (
    <div className={styles.home_wrap}>
      {!(global as any).isMobile && <Header />}
      {(global as any).isMobile && <Bottom back={articleFile.data.pathInfo.name ? true : false} />}
      <div className={styles.home_content}>
        {props.articleFile.init && (
          <div>
            {articleFile.data.pathInfo.name && (
              <div className={styles.title}>
                <div className={styles.rightTitle}>
                  <p className={styles.name}>{articleFile.data.pathInfo.name}</p>
                  <p className={styles.total}>篇数: {articleFile.data.page.total}</p>
                </div>
              </div>
            )}
            <Tloader
              className={styles.tLoader}
              onLoadMore={loadMore}
              autoLoadMore={true}
              hasMore={articleFile.data.page.total > articleFile.data.datas.length + articleFile.data.page.offset}
              initializing={2}
            >
              {generateRecomand(articleFile.data.datas)}
            </Tloader>
          </div>
        )}
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  personPublishedFile: state.article.personPublishedFile,
  searchWord: state.article.searchWord,
  articleFile: state.article.articleFile,
  articleFolder: state.article.articleFolder
})

const mapDispatchToProps = (dispatch: any) => ({
  getPublishedFile: dispatch.article.getPublishedFile,
  getArticleFile: dispatch.article.getArticleFile,
  getPersonPublishedFile: dispatch.article.getPersonPublishedFile,
  UpdateArticleThumbsUpNumById: dispatch.article.UpdateArticleThumbsUpNumById
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PersonalArt)
)
