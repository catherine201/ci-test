import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Tloader from 'react-touch-loader'
import styles from './index.module.less'
import constant from '@/utils/constant.js'
import { Carousel, Icon } from 'antd'
import Header from '@/components/header.tsx'
import Bottom from '@/components/bottom.tsx'
import { wrapRed, handleAbstract, getDateDiff } from '@/utils/index.js'
import store from '@/store/index.js'

const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_795441_mxj9e6qdfxq.js'
})
const add = (global as any).isMobile ? 30 : 100
const Dashboard = (props: any) => {
  const { publishedFile, getPublishedFile, isLogin } = props
  const { init } = publishedFile

  useEffect(() => {
    const initFuc = () => {
      if (!publishedFile.init) {
        getPublishedFile()
      }
      window.onbeforeunload = () => {
        sessionStorage.setItem('store', JSON.stringify(store.getState()))
        getPublishedFile()
      }
    }
    initFuc()
  }, [publishedFile, getPublishedFile, isLogin, init])

  const carousels = [1, 2, 3, 4]

  const toDetail = (val: any) => {
    if ((global as any).isMobile) {
      props.history.push(`/art/articleContent/${val}`)
    } else {
      window.open(window.origin + `/#/art/articleContent/${val}`)
    }
  }

  const toAuthor = (val: any, name: any) => {
    const obj = {
      openid: val
    }
    props.getPersonPublishedFile(obj).then(() => {
      props.history.push(`/art/personalArt/${val}/${name}`)
    })
  }

  const loadMore = () => {
    const obj = {
      limit: constant.LIMIT,
      offset: props.publishedFile.data.page.offset + props.publishedFile.data.datas.length,
      add: 'add'
    }
    props.getPublishedFile(obj)
  }

  const changeLike = (e, menu) => {
    const obj = {
      id: menu._id
    }
    if (menu.thumb_status) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).status = false
    }
    console.log(e.currentTarget.children[1])
    if (!menu.thumb_status) {
      e.currentTarget.children[1].className = 'i slideInUp'
    } else {
      e.currentTarget.children[1].className = 'i'
    }
    props.UpdateArticleThumbsUpNumById(obj).then(() => {
      const sendObj = {
        limit: props.publishedFile.data.datas.length,
        offset: 0
      }
      getPublishedFile(sendObj)
    })
  }

  const generateRecomand = (menus: any) => {
    let items = []
    items = menus.map(menu => (
      <div key={menu._id} className={styles.list_item}>
        <div className={styles.list_item_left}>
          <div className={styles.top_title}>
            {menu.paths && menu.paths[0] && (
              <div className={styles.path}>{menu.paths && menu.paths[0] && menu.paths[0].pathName}</div>
            )}
            <p
              className={styles.title}
              onClick={() => {
                toDetail(menu._id)
              }}
              dangerouslySetInnerHTML={{ __html: wrapRed(menu.title, props.searchWord) as any }}
            />
          </div>
          <div dangerouslySetInnerHTML={{ __html: handleAbstract(menu.content, props.searchWord, add) as any }} />
          <div className={styles.desc}>
            <span
              className={`${styles.author} ${styles.viewCount}`}
              onClick={() => {
                toAuthor(menu.openid, menu.author)
              }}
            >
              <Icon type="user" className={styles.icon} />
              {menu.author}
            </span>
            <span>
              <IconFont
                type="hd-icon-index-shijian"
                className={styles.icon}
                style={{ fontSize: '16px', color: '#999' }}
              />
              {menu.updated_at &&
                !(global as any).isMobile &&
                (new Date(menu.updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
              {menu.updated_at && (global as any).isMobile && getDateDiff(menu.updated_at)}
            </span>
            <span>
              <Icon type="eye" className={styles.icon} />
              {menu.view_count}浏览
            </span>
            <span
              className={`${styles.viewCount} ${styles.thumbs}`}
              onClick={e => {
                changeLike(e, menu)
              }}
            >
              <Icon type="like" className={menu.thumb_status ? `${styles.icon} ${styles.active}` : `${styles.icon}`} />
              {menu.thumbsUp_count - 0 > 0 ? menu.thumbsUp_count : ''}
              <i className="i">+1</i>
            </span>
            <span
              className={styles.viewCount}
              onClick={() => {
                if ((global as any).isMobile) {
                  props.history.push(`/art/articleContent/${menu._id}#comments`)
                } else {
                  window.open(window.origin + `/#/art/articleContent/${menu._id}#comments`)
                }
              }}
            >
              <Icon type="message" className={styles.icon} style={{ color: '#1DA57A' }} />
              {menu.comment.length}
              {!(global as any).isMobile && '评论'}
            </span>
          </div>
        </div>
        {!(global as any).isMobile && (
          <div className={styles.img_wrap}>
            {menu.pictureUrl ? (
              <img
                src={menu.pictureUrl}
                alt=""
                className={styles.img}
                onClick={() => {
                  toDetail(menu._id)
                }}
              />
            ) : (
              ''
            )}
          </div>
        )}
      </div>
    ))
    return items
  }

  const generateCarousel = (menus: any) => {
    let items = []
    items = menus.map((menu: any, index: number) => (
      <div key={index}>
        <img src={`http://${constant.ip}/banner${index + 1}.jpeg`} alt="" className={styles.carouselImg} />
      </div>
    ))
    return items
  }

  return (
    <div className={`home_wrap ${styles.home_conetent_wrap}`}>
      {!(global as any).isMobile && <Header />}
      {(global as any).isMobile && <Bottom />}
      <div className={styles.home_content}>
        {!(global as any).isMobile && (
          <Carousel effect="fade" autoplay={true} className={styles.carousel} speed={10}>
            {generateCarousel(carousels)}
          </Carousel>
        )}
        {props.publishedFile.init && (
          <Tloader
            className={styles.tLoader}
            onLoadMore={loadMore}
            autoLoadMore={true}
            hasMore={
              props.publishedFile.data.page.total >
              props.publishedFile.data.datas.length + props.publishedFile.data.page.offset
            }
            initializing={2}
          >
            {generateRecomand(props.publishedFile.data.datas)}
          </Tloader>
        )}
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  isLogin: state.account.isLogin,
  articleFile: state.article.articleFile,
  articleFolder: state.article.articleFolder,
  publishedFile: state.article.publishedFile,
  searchWord: state.article.searchWord
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFile: dispatch.article.getArticleFile,
  getArticleFolder: dispatch.article.getArticleFolder,
  getArticleFileById: dispatch.article.getArticleFileById,
  getPersonPublishedFile: dispatch.article.getPersonPublishedFile,
  getPublishedFile: dispatch.article.getPublishedFile,
  clearFileContent: dispatch.article.clearFileContent,
  UpdateArticleThumbsUpNumById: dispatch.article.UpdateArticleThumbsUpNumById
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Dashboard)
)
