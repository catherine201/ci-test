import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Bottom from '@/components/bottom.tsx'
import styles from './index.module.less'
import { Avatar, Icon } from 'antd'

const menuList = [
  {
    key: 'basic',
    path: '/setting/basic',
    text: '基础设置',
    icon: 'menu'
  },
  {
    key: 'changePsw',
    path: '/setting/changePsw',
    text: '更改密码',
    icon: 'sync'
  }
  // {
  //   key: 'logout',
  //   text: '退出账号',
  //   icon: 'logout'
  // }
]
const Mine = props => {
  useEffect(() => {
    console.log('componentDidMount: 组件加载后')
    // loading.start()
    return () => {
      console.log('componentWillUnmount: 组件卸载， 做一些清理工作')
    }
  }, [])

  useEffect(() => {
    console.log('componentDidUpdate： 更新usernmae')
  }, [])

  // 退出登录
  const logOut = () => {
    props.setIsLoginAction(false)
    props.setUserInfoAction({})
  }

  const toHref = val => {
    props.history.push(val)
  }

  const generateList = (menus: any) => {
    let items = []
    items = menus.map((menu: any) => (
      <li
        key={menu.key}
        className={styles.setting_list_li}
        onClick={() => {
          toHref(menu.path)
        }}
      >
        {/* style={{ fontSize: '28px', color: '#666464' }} */}
        <Icon type={menu.icon} className={styles.icon} />
        <span>{menu.text}</span>
      </li>
    ))
    return items
  }

  return (
    <div className={styles.me_wrap}>
      <Bottom />
      <div className={styles.top_title}>我</div>
      <div className={styles.login_info}>
        {props.isLogin && props.userInfo ? (
          <div className={styles.login}>
            <img src={props.userInfo.avator} className={styles.Avatar} alt="头像" />
            <span>{props.userInfo.name}</span>
          </div>
        ) : (
          <div className={styles.login}>
            <Avatar size={40} icon="user" className={styles.Avatar} />
            <span
              onClick={() => {
                toHref('/login?flag=me')
              }}
            >
              登录 / 注册
            </span>
          </div>
        )}
      </div>
      <ul className={styles.setting_list}>{generateList(menuList)}</ul>
      <div
        className={styles.logout}
        onClick={() => {
          logOut()
        }}
      >
        退出账号
      </div>
    </div>
  )
}
// export default Mine

const mapStateToProps = (state: any) => ({
  isLogin: state.account.isLogin,
  userInfo: state.account.userInfo
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFile: dispatch.article.getArticleFile,
  setIsLoginAction: dispatch.account.setIsLoginAction,
  setUserInfoAction: dispatch.account.setUserInfoAction
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Mine)
)
