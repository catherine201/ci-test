import React, { useEffect } from 'react'
import Header from '@/components/header.tsx'
import styles from './index.module.less'
import apliPayImg from '@/assets/images/alipay.png'
import Message from '@/components/message/index.tsx'

const wechatList = [
  {
    imgSrc: require('@/assets/images/1m.png'),
    money: '1'
  },
  {
    imgSrc: require('@/assets/images/5m.png'),
    money: '5'
  },
  {
    imgSrc: require('@/assets/images/10m.png'),
    money: '10'
  },
  {
    imgSrc: require('@/assets/images/any.png'),
    money: 'Any'
  }
]
const Sponsor = () => {
  useEffect(() => {
    console.log('init')
  }, [])

  const generateWechat = (menus: any) => {
    let items = []
    // tslint:disable-next-line: no-shadowed-variable
    items = menus.map((menu, index) => (
      <li key={index} className={styles.wechat_pay_li}>
        <a href="/" className={styles.li_a}>
          <span>微信扫一扫转账</span>
          <br />
          <img src={menu.imgSrc} className={styles.wechat_img} alt="微信转账" />
          <br />
          <span>¥{menu.money}</span>
          <span className={styles.grey} />
        </a>
      </li>
    ))
    return items
  }

  return (
    <div className={styles.sponsor_wrap}>
      <Header />
      <div className={styles.sponsor_content_wrap}>
        <div className="common_title">赞助作者</div>
        <div className={styles.sponsor_content}>
          <div className="common_p">
            如果您喜欢我的文章，感觉我的文章对您有帮助，不妨动动您的金手指给予小额赞助，予人玫瑰，手有余香，不胜感激。
          </div>
          <div className="common_p">赞助方式一</div>
          <div className={styles.alipay_method}>
            <div>手机支付宝扫一扫</div>
            <img src={apliPayImg} alt="手机支付宝" className={styles.alipay_img} />
          </div>
          <div className="common_p">赞助方式二</div>
          <div className={styles.wechat_method}>
            <div>微信扫一扫</div>
            {/* <img src="" alt="手机支付宝" /> */}
            <ul className={styles.wechat_pay_list}>{generateWechat(wechatList)}</ul>
          </div>
        </div>
        <Message />
      </div>
    </div>
  )
}
export default Sponsor
