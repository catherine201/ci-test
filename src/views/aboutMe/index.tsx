import React, { useEffect } from 'react'
import Header from '@/components/header.tsx'
import styles from './index.module.less'
import { Icon } from 'antd'
import Message from '@/components/message/index.tsx'

const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_795441_oxvrei4jnr.js'
})

const contractList = [
  {
    name: 'github',
    icon: 'github',
    link: 'https://github.com/catherine201?tab=repositories',
    color: '#7884cb'
  },
  {
    name: 'jj',
    iconFont: 'hd-icon-index-juejin',
    link: 'https://juejin.im/user/5c03296fe51d451aa843c033',
    color: '#317de5'
  },
  {
    name: 'wb',
    icon: 'weibo-circle',
    link: 'https://weibo.com/2907751065/profile?topnav=1&wvr=6&is_all=1',
    color: '#e98b88'
  },
  {
    name: 'zh',
    icon: 'zhihu',
    link: 'https://www.zhihu.com/people/catherine2020/activities',
    color: '#3885e0'
  }
]

const Me = () => {
  useEffect(() => {
    console.log('init')
  }, [])

  const generateIcon = (menus: any) => {
    let items = []
    // tslint:disable-next-line: no-shadowed-variable
    items = menus.map((menu, index) => (
      <li
        key={index}
        className={styles.icon_li}
        onClick={() => {
          window.open(menu.link)
        }}
      >
        {menu.icon ? (
          <Icon type={menu.icon} style={{ fontSize: '70px', color: menu.color }} />
        ) : (
          <IconFont type={menu.iconFont} className={styles.icon} style={{ fontSize: '70px', color: menu.color }} />
        )}
      </li>
    ))
    return items
  }

  return (
    <div className={styles.me_wrap}>
      <Header />
      <div className={styles.me_content_wrap}>
        <div className="common_title">关于博主</div>
        <div className={styles.me_content}>
          <div className="common_p">个人简介</div>
          <div className={styles.basic_content}>
            <div className={styles.avator} />
            <p className={styles.name}>颜如冰</p>
            <div>
              <p>前端开发工程师</p>
              <p>技术栈: React、Vue、Node 等</p>
              <p>爱生活、爱做饭、爱运动、爱编程、爱挑战</p>
              <p>人生很短、人生很长、活在当下</p>
              <p>humble、humble、humble</p>
            </div>
          </div>
          <div className="common_p">与我联系</div>
          <div className={styles.contract_me_wrap}>
            <ul className={styles.contract_me_ul}>{generateIcon(contractList)}</ul>
          </div>
        </div>
        <Message />
      </div>
    </div>
  )
}
export default Me
