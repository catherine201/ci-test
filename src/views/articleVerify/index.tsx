import React, { useEffect, useState } from 'react'
import { Table, Button, message, Modal, Radio, Checkbox } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from './index.module.less'
import { getParams } from '@/utils/index.js'

const { confirm } = Modal
const LIMIT = 10
const ArticleVerify = props => {
  const [chooseRadio, setchooseRadio] = useState(1) // 1 通过审核 status 1 0 表示驳回, status 0
  const [ids, setids] = useState([]) // 1 通过审核 status 1 0 表示驳回, status 0
  const { getAllArticleFile, userInfo, allArticleFile, pagination, getPagination, setSearchWordAction } = props
  const outerInit = () => {
    const obj = {}
    const status = getParams('status')
    if (status - 0 !== 4) {
      // tslint:disable-next-line: semicolon
      ;(obj as any).status = status + ''
    }
    getAllArticleFile(obj)
  }
  useEffect(() => {
    const init = () => {
      const obj = {}
      const status = getParams('status')
      if (status - 0 !== 4) {
        // tslint:disable-next-line: semicolon
        ;(obj as any).status = status + ''
      }
      getAllArticleFile(obj)
    }
    init()
    return () => {
      setSearchWordAction('')
    }
  }, [getAllArticleFile, setSearchWordAction])

  const queryArr = [
    {
      name: '全部',
      key: 4
    },
    {
      name: '已发布',
      key: 1
    },
    {
      name: '草稿',
      key: 0
    },
    {
      name: '待审',
      key: 2
    }
  ]

  const changeLink = key => {
    const obj = {}
    if (key !== 4) {
      // tslint:disable-next-line: semicolon
      ;(obj as any).status = key + ''
    }
    getAllArticleFile(obj).then(() => {
      props.history.push(`/verify/article?status=${key}`)
    })
  }

  const generateList = menus => {
    let items = []
    items = menus.map(menu => (
      <li
        key={menu.key}
        className={`${styles.menu_item}
          ${getParams('status') - 0 === menu.key && styles.menu_item_active}`}
        onClick={() => {
          changeLink(menu.key)
        }}
      >
        {menu.name}
      </li>
    ))
    return items
  }
  // 翻页
  const handleTableChange = page => {
    console.log(page)
    const pager = { ...page }
    const obj = {
      limit: LIMIT,
      offset: (page.current - 1) * LIMIT
    }
    getAllArticleFile(obj)
    getPagination(pager)
  }

  const onChangeRadio = e => {
    console.log('radio checked', e.target.value)
    setchooseRadio(e.target.value)
  }

  const changeStatus = (id, status) => {
    const obj = {
      id: [id],
      status
    }
    props.UpdateArticleStatusById(obj).then(() => {
      outerInit()
    })
  }

  // 修改是否置顶
  const onchangeTop = (e, data) => {
    const obj = {
      id: data._id,
      toTop: e.target.checked ? 1 : 0
    }
    console.log(obj)
    props.UpdateArticleTopById(obj).then(() => {
      outerInit()
    })
  }

  const columns = [
    {
      title: '标题',
      // dataIndex: 'title',
      // key: 'title',
      width: '15%',
      render: text =>
        text ? (
          <span
            className={styles.title_span}
            onClick={() => {
              window.open(window.origin + `/#/art/articleContent/${text._id}`)
            }}
          >
            {text.title}
          </span>
        ) : (
          ''
        )
    },
    {
      title: '作者',
      dataIndex: 'author',
      key: 'author',
      width: '10%'
    },
    {
      title: '分类目录',
      dataIndex: 'pathId.pathName',
      key: 'pathId.pathName',
      width: '16%'
    },
    // {
    //   title: '摘要',
    //   dataIndex: 'abstract',
    //   key: 'abstract',
    //   width: '36%'
    // },
    {
      title: '创建时间',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => (text ? (new Date(text) as any).format('yyyy-MM-dd hh:mm:ss') : ''),
      width: '18%'
    },
    {
      title: '更新时间',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: text => (text ? (new Date(text) as any).format('yyyy-MM-dd hh:mm:ss') : ''),
      width: '18%'
    },
    {
      title: '是否置顶',
      render: text => (
        <Checkbox
          onChange={e => {
            onchangeTop(e, text)
          }}
          checked={text.toTop === 1}
          disabled={userInfo.type !== 'super'}
        />
      )
    }
  ]

  if (getParams('status') === '4' || getParams('status') === '2') {
    // tslint:disable-next-line: whitespace
    ;(columns as any).push({
      title: '编辑',
      // align: 'center',
      render: text => {
        return text.status === 2 ? (
          <div>
            <Button
              type="primary"
              onClick={() => {
                // handleDelete(text)
                changeStatus(text._id, 1)
              }}
              className={styles.table_mr5}
            >
              通过
            </Button>
            <Button
              type="danger"
              onClick={() => {
                changeStatus(text._id, 0)
              }}
            >
              驳回
            </Button>
          </div>
        ) : (
          ''
        )
      }
    })
  }

  const rowSelection = {
    onChange: selectedRowKeys => {
      console.log(selectedRowKeys)
      setids(selectedRowKeys)
      // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows)
    },
    getCheckboxProps: record => ({
      disabled: record.status !== 2 // Column configuration not to be checked
    })
  }

  const audit = () => {
    console.log(ids)
    if (ids.length) {
      confirm({
        title: '文章审核',
        icon: 'check-circle',
        content: (
          // onChange={onChangeRadio} value={chooseRadio}
          <Radio.Group onChange={onChangeRadio} defaultValue={chooseRadio}>
            <Radio value={1}>通过</Radio>
            <Radio value={0}>驳回</Radio>
          </Radio.Group>
        ),
        onOk() {
          console.log('OK')
          const obj = {
            id: ids,
            status: chooseRadio
          }
          props.UpdateArticleStatusById(obj).then(() => {
            outerInit()
          })
        },
        onCancel() {
          console.log('Cancel')
        }
      })
    } else {
      message.warning('请先选择要审核的文章')
      return
    }
  }

  return (
    <div className={styles.verify_wrap}>
      <div className={styles.top_line}>
        <ul className={styles.list_wrap}>{generateList(queryArr)}</ul>
        {getParams('status') === '4' || getParams('status') === '2' ? (
          <Button
            type="primary"
            onClick={() => {
              audit()
            }}
          >
            批量审核
          </Button>
        ) : (
          <span />
        )}
      </div>

      {getParams('status') === '4' || getParams('status') === '2' ? (
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={allArticleFile.data && allArticleFile.data.datas}
          className={styles.table}
          rowKey={record => {
            return (record as any)._id
          }}
          pagination={pagination}
          // tslint:disable-next-line: no-shadowed-variable
          onChange={pagination => {
            handleTableChange(pagination)
          }}
        />
      ) : (
        <Table
          columns={columns}
          dataSource={allArticleFile.data && allArticleFile.data.datas}
          className={styles.table}
          rowKey={record => {
            return (record as any)._id
          }}
          pagination={pagination}
          // tslint:disable-next-line: no-shadowed-variable
          onChange={pagination => {
            handleTableChange(pagination)
          }}
        />
      )}
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  userInfo: state.account.userInfo,
  pagination: state.searchOption.pagination,
  searchWord: state.article.searchWord,
  allArticleFile: state.article.allArticleFile
})

const mapDispatchToProps = (dispatch: any) => ({
  getAllArticleFile: dispatch.article.getAllArticleFile,
  getPagination: dispatch.searchOption.getPagination,
  resetPagination: dispatch.searchOption.resetPagination,
  setSearchWordAction: dispatch.article.setSearchWordAction,
  UpdateArticleStatusById: dispatch.article.UpdateArticleStatusById,
  UpdateArticleTopById: dispatch.article.UpdateArticleTopById
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ArticleVerify)
)
