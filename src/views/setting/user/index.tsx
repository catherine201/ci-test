import React, { useEffect, useState } from 'react'
import { Table, Input, Button, Checkbox } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from '../index.module.less'
const { Search } = Input
const LIMIT = 10
const User = props => {
  const {
    GetAllUser,
    userList,
    userInfo,
    UpdateType,
    DeleteUserByOpenId,
    userSearchWord,
    setUserSearchWordAction,
    pagination,
    getPagination
  } = props
  const [word, setword] = useState('') // 搜索
  useEffect(() => {
    const init = () => {
      GetAllUser()
      setword(userSearchWord)
    }
    init()
  }, [GetAllUser, userSearchWord])
  // 修改权限
  const onchange = (e, data) => {
    const obj = {
      userOpenId: data.openid,
      admin: e.target.checked
    }
    UpdateType(obj)
  }
  // 翻页
  const handleTableChange = page => {
    console.log(page)
    const pager = { ...page }
    const obj = {
      limit: LIMIT,
      offset: (page.current - 1) * LIMIT
    }
    GetAllUser(obj)
    getPagination(pager)
  }

  // 搜索
  const handleSearch = value => {
    setUserSearchWordAction(value).then(() => {
      props.resetPagination()
      //   GetAllUser()
    })
  }

  // 删除
  const handleDelete = text => {
    console.log(text)
    const obj = {
      name: text.name
    }
    DeleteUserByOpenId(obj)
  }
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'nickName',
      dataIndex: 'nickName',
      key: 'nickName'
    },
    {
      title: '创建时间',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => (text ? (new Date(text) as any).format('yyyy-MM-dd hh:mm:ss') : '')
    },
    {
      title: 'avator',
      dataIndex: 'avator',
      key: 'avator',
      render: text => (text ? <img src={text} className={styles.avator} alt="头像" /> : '')
    },
    {
      title: '是否admin',
      render: text => (
        <Checkbox
          onChange={e => {
            onchange(e, text)
          }}
          checked={text.type === 'admin' || text.type === 'super'}
          disabled={userInfo.type !== 'super'}
        />
      )
    }
  ]
  if (userInfo.type === 'super') {
    columns.push({
      title: '编辑',
      render: text => (
        <Button
          onClick={() => {
            handleDelete(text)
          }}
        >
          删除
        </Button>
      )
    })
  }
  return (
    <div className={styles.user_wrap}>
      <div className={styles.search_wrap}>
        <Search
          placeholder="搜索"
          onSearch={value => handleSearch(value)}
          style={{ width: 200 }}
          value={word}
          onChange={e => {
            setword(e.target.value)
          }}
          allowClear={true}
        />
      </div>

      <Table
        columns={columns}
        dataSource={userList.data && userList.data.datas}
        className={styles.table}
        rowKey={record => {
          return (record as any).openid
        }}
        pagination={pagination}
        // tslint:disable-next-line: no-shadowed-variable
        onChange={pagination => {
          handleTableChange(pagination)
        }}
      />
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  userInfo: state.account.userInfo,
  userList: state.account.userList,
  userSearchWord: state.account.userSearchWord,
  pagination: state.searchOption.pagination
})

const mapDispatchToProps = (dispatch: any) => ({
  GetAllUser: dispatch.account.GetAllUser,
  UpdateType: dispatch.account.UpdateType,
  DeleteUserByOpenId: dispatch.account.DeleteUserByOpenId,
  setUserSearchWordAction: dispatch.account.setUserSearchWordAction,
  getPagination: dispatch.searchOption.getPagination,
  resetPagination: dispatch.searchOption.resetPagination
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(User)
)
