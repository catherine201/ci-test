import React, { useEffect, useState } from 'react'
import { Upload, Button, Avatar, Input, Icon } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import styles from '../index.module.less'
import { getToken } from '@/api/axios.js'
import constant from '@/utils/constant.js'

const Basic = props => {
  const [avatorUrl, setavatorUrl] = useState('') // 头像
  const [nickName, setnickName] = useState('') // 昵称
  const { userInfo } = props
  useEffect(() => {
    const init = () => {
      if (userInfo && userInfo.avator) {
        setavatorUrl(userInfo.avator)
      }
      if (userInfo && userInfo.nickName) {
        setnickName(userInfo.nickName)
      }
    }
    init()
  }, [userInfo])
  const uploadProps = {
    action: `//${constant.ip}/common/uploadFile`,
    showUploadList: false,
    headers: {
      Authorization: getToken()
    },
    onChange: (res: any) => {
      if (res.file && res.file.response && res.file.response.status === 200) {
        setavatorUrl(res.file.response.data.url)
      }
    }
  }
  const save = () => {
    console.log('save')
    const obj: any = {
      avator: avatorUrl
    }
    if (nickName) {
      obj.nickName = nickName
    }
    props.UpdateUserInfo(obj)
  }
  return (
    <div className={styles.basic_wrap}>
      {(global as any).isMobile && (
        <div className="top_back">
          <Icon
            type="left"
            style={{ fontSize: '16px', color: '#007fff' }}
            className="icon_back"
            onClick={() => {
              console.log('click')
              props.history.go(-1)
            }}
          />
          基础设置
        </div>
      )}
      <div className={styles.avator_wrap}>
        {avatorUrl ? (
          <img src={avatorUrl} className={styles.Avatar} alt="头像" />
        ) : (
          <Avatar className={styles.Avatar} />
        )}
        <Upload className="avatar-uploader" {...uploadProps}>
          <Button type="primary" shape="round">
            {avatorUrl ? '更改头像' : '设置头像'}
          </Button>
        </Upload>
      </div>
      <div className={styles.nick_wrap}>
        <div className={styles.nickName}>昵称</div>
        <Input
          value={nickName}
          onChange={e => {
            setnickName(e.target.value)
          }}
        />
      </div>
      <Button
        type="primary"
        shape="round"
        onClick={() => {
          save()
        }}
      >
        保存
      </Button>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  userInfo: state.account.userInfo
})

const mapDispatchToProps = (dispatch: any) => ({
  UpdateUserInfo: dispatch.account.UpdateUserInfo
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Basic)
)
