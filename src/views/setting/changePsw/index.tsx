import React, { useState } from 'react'
import { Button, Form, Input, message, Icon } from 'antd'
import { withRouter } from 'react-router-dom'
import { regular } from '@/utils/validate.js'
import { connect } from 'react-redux'
import styles from '../index.module.less'

const ChangePsw = props => {
  const [confirmDirty, setconfirmDirty] = useState(false)
  const { getFieldDecorator } = props.form
  const handleChange = e => {
    e.preventDefault()
    props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const obj = values
        props.UpdatePassword(obj).then(() => {
          message.success('成功修改密码')
          props.form.resetFields()
        })
      }
    })
  }

  const handleConfirmBlur = (e: { target: { value: any } }) => {
    const value = e.target.value
    setconfirmDirty(confirmDirty || !!value)
  }

  // tslint:disable-next-line: unified-signatures
  const validateToNextPassword = (rule: any, value: any, callback: { (arg0: string): void; (): void }) => {
    const form = props.form
    if (value && confirmDirty) {
      form.validateFields(['confirmPassword'], { force: true })
    }
    if (value && !regular.passWord.test(value)) {
      callback('密码至少为8位的字母,数字,字符任意两种的组合!')
    } else {
      callback()
    }
  }

  // tslint:disable-next-line: unified-signatures
  const compareToFirstPassword = (rule: any, value: any, callback: { (arg0: string): void; (): void }) => {
    const form = props.form
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('两次输入的密码不一致!')
    } else {
      callback()
    }
  }

  const validateToOldPassword = (rule, value, callback) => {
    if (value && !regular.passWord.test(value)) {
      callback('密码至少为8位的字母,数字,字符任意两种的组合!')
    } else {
      callback()
    }
  }

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 }
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 }
    }
  }

  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0
      },
      sm: {
        span: 16,
        offset: 8
      }
    }
  }

  return (
    <div className={styles.changePsw_wrap}>
      {(global as any).isMobile && (
        <div className="top_back">
          <Icon
            type="left"
            style={{ fontSize: '16px', color: '#007fff' }}
            className="icon_back"
            onClick={() => {
              console.log('click')
              props.history.go(-1)
            }}
          />
          更改密码
        </div>
      )}
      <Form onSubmit={handleChange} {...formItemLayout}>
        <Form.Item label="旧密码">
          {getFieldDecorator('oldPassword', {
            rules: [
              { required: true, message: '请输入旧密码!' },
              {
                validator: validateToOldPassword
              }
            ]
          })(<Input type="password" className={styles.w250} />)}
        </Form.Item>
        <Form.Item label="新密码">
          {getFieldDecorator('newPassword', {
            rules: [
              { required: true, message: '请输入您要设置的新密码!' },
              {
                validator: validateToNextPassword
              }
            ]
          })(<Input type="password" className={styles.w250} />)}
        </Form.Item>
        <Form.Item label="确认新密码">
          {getFieldDecorator('confirmPassword', {
            rules: [
              {
                required: true,
                message: '请再次输入密码!'
              },
              {
                validator: compareToFirstPassword
              }
            ]
          })(<Input type="password" onBlur={handleConfirmBlur} className={styles.w250} />)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" shape="round">
            更改密码
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
// export default ChangePsw

const mapStateToProps = (state: any) => ({
  snow: state.animate.snow
})

const mapDispatchToProps = (dispatch: any) => ({
  UpdatePassword: dispatch.account.UpdatePassword
})
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Form.create()(ChangePsw))
)
