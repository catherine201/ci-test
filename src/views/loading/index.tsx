import React, { useEffect } from 'react'
import styles from './index.module.less'

const Loading = () => {
  useEffect(() => {
    console.log('componentDidMount: 组件加载后')
    // loading.start()
    return () => {
      console.log('componentWillUnmount: 组件卸载， 做一些清理工作')
    }
  }, [])

  useEffect(() => {
    console.log('componentDidUpdate： 更新usernmae')
  }, [])

  return (
    <div className={styles.loading_wrap}>
      <img src={require('@/assets/images/loading_web.gif')} alt="" />
    </div>
  )
}
export default Loading
