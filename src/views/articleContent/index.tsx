import React, { useEffect, useState } from 'react'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { withRouter } from 'react-router-dom'
import styles from './index.module.less'
import { Icon, Input } from 'antd'
import { connect } from 'react-redux'
import Message from '@/components/message/index.tsx'
import IssueComment from './issueComment.tsx'

const ArticleContent = (props: any) => {
  const { getArticleFileById, match } = props
  const { id } = match.params
  const [show, setshow] = useState(false) // 控制评论的显示
  useEffect(() => {
    getArticleFileById(id)
  }, [getArticleFileById, id])

  const changeLike = (e, menu) => {
    const obj = {
      id: menu._id
    }
    if (menu.thumb_status) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).status = false
    }
    console.log(e.currentTarget.children[1])
    if (!menu.thumb_status) {
      e.currentTarget.children[1].className = 'i slideInUp'
    } else {
      e.currentTarget.children[1].className = 'i'
    }
    props.UpdateArticleThumbsUpNumById(obj).then(() => {
      getArticleFileById(id)
      const sendObj = {
        limit: props.publishedFile.data.datas.length,
        offset: 0
      }
      props.getPublishedFile(sendObj)
    })
  }

  const finishSubmit = val => {
    setshow(false)
    const sendObj = {
      limit: props.publishedFile.data.datas.length,
      offset: 0
    }
    props.getPublishedFile(sendObj)
    // finishSubmit(JSON.parse(JSON.stringify(val)))
  }

  return (
    <div className={styles.article_content_wrap}>
      {(global as any).isMobile && (
        <div className="top_back">
          <Icon
            type="left"
            style={{ fontSize: '16px', color: '#007fff' }}
            className="icon_back"
            onClick={() => {
              console.log('click')
              props.history.go(-1)
            }}
          />
          文章详情
        </div>
      )}
      {props.articleFileContent && props.articleFileContent.data ? (
        <div className={styles.article_content}>
          <div className={styles.fixed_left}>
            <div
              className={`${styles.viewCount} ${styles.thumbs}`}
              onClick={e => {
                changeLike(e, props.articleFileContent.data)
              }}
            >
              <Icon
                type="like"
                className={
                  props.articleFileContent.data.thumb_status ? `${styles.icon} ${styles.active}` : `${styles.icon}`
                }
              />
              <i className="i">+1</i>
              <span className={styles.count}>
                {props.articleFileContent.data.thumbsUp_count - 0 > 0
                  ? props.articleFileContent.data.thumbsUp_count
                  : ''}
              </span>
            </div>
            <div
              className={styles.viewCount}
              onClick={() => {
                props.history.push(`/art/articleContent/${props.articleFileContent.data._id}#comments`)
                // if ((global as any).isMobile) {
                //   props.history.push(`/art/articleContent/${props.articleFileContent.data._id}#comments`)
                // } else {
                //   window.open(window.origin + `/#/art/articleContent/${props.articleFileContent.data._id}#comments`)
                // }
              }}
            >
              <Icon type="message" className={styles.icon} style={{ color: '#1DA57A' }} />
              <span className={styles.count}>{props.articleFileContent.data.comment_count}</span>
            </div>
          </div>
          <div className={styles.title}>{props.articleFileContent.data.title}</div>
          <div className={styles.desc}>
            <span className={styles.pathName}>{props.articleFileContent.data.pathId.pathName}</span>
            <span className={styles.author}>{props.articleFileContent.data.author}</span>
            <span>
              {props.articleFileContent.data.updated_at &&
                (new Date(props.articleFileContent.data.updated_at) as any).format('yyyy-MM-dd hh:mm:ss')}
            </span>
            <span className={styles.viewCount}>{props.articleFileContent.data.view_count}浏览</span>
          </div>
          <div dangerouslySetInnerHTML={{ __html: props.articleFileContent.data.content }} />
        </div>
      ) : (
        ''
      )}
      <div className={styles.border} />
      {!show && <Message />}
      {(global as any).isMobile && (
        <div className={styles.bottom_comment}>
          <Input
            placeholder="输入评论……"
            className={styles.comment_input}
            onFocus={() => {
              console.log('输入评论')
              setshow(true)
            }}
          />
          <div className={styles.rigth}>
            <span
              className={`${styles.viewCount} ${styles.thumbs}`}
              onClick={e => {
                changeLike(e, props.articleFileContent.data)
              }}
            >
              <Icon
                type="like"
                className={
                  props.articleFileContent &&
                  props.articleFileContent.data &&
                  props.articleFileContent.data.thumb_status
                    ? `${styles.icon} ${styles.active}`
                    : `${styles.icon}`
                }
              />
              {props.articleFileContent.data && props.articleFileContent.data.thumbsUp_count - 0 > 0
                ? props.articleFileContent.data.thumbsUp_count
                : ''}
              <i className="i">+1</i>
            </span>
            <span
              className={styles.viewCount}
              onClick={() => {
                if ((global as any).isMobile) {
                  props.history.push(`/art/articleContent/${props.articleFileContent.data._id}#comments`)
                } else {
                  window.open(window.origin + `/#/art/articleContent/${props.articleFileContent.data._id}#comments`)
                }
              }}
            >
              <Icon type="message" className={styles.icon} style={{ color: '#1DA57A' }} />
              {props.articleFileContent.data && props.articleFileContent.data.comment_count}
              {!(global as any).isMobile && '评论'}
            </span>
          </div>
        </div>
      )}
      {show && (
        <TransitionGroup>
          <CSSTransition key="message" classNames="fade" timeout={500}>
            <IssueComment finishSubmit={finishSubmit} />
          </CSSTransition>
        </TransitionGroup>
      )}
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  publishedFile: state.article.publishedFile,
  articleFileContent: state.article.articleFileContent
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFileById: dispatch.article.getArticleFileById,
  getPublishedFile: dispatch.article.getPublishedFile,
  UpdateArticleThumbsUpNumById: dispatch.article.UpdateArticleThumbsUpNumById
})
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ArticleContent)
)
