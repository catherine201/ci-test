import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import styles from './index.module.less'
import { Icon, Input, Modal } from 'antd'
import { connect } from 'react-redux'

const { TextArea } = Input
const { confirm } = Modal

const IssueComment = (props: any) => {
  const [word, setword] = useState('') // 搜索
  const ref = React.createRef()
  useEffect(() => {
    // tslint:disable-next-line: whitespace
    ;(ref as any).current.focus()
  }, [])

  const submitComment = () => {
    const obj = {
      comment: word
    }
    // if (props.pid) {
    //   // tslint:disable-next-line: whitespace
    //   ;(obj as any).pid = props.pid
    // }
    if (props.location.pathname.split('/')[1]) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).type = props.location.pathname.split('/')[1]
    }
    if (props.match.params.id) {
      // tslint:disable-next-line: whitespace
      ;(obj as any).articleId = props.match.params.id
    }
    props.createComment(obj).then(res => {
      setword('')
      props.finishSubmit()
    })
  }

  const showConfirm = () => {
    console.log('confirm')
    confirm({
      title: '确定退出评论页面?',
      //   content: 'Some descriptions',
      onOk() {
        console.log('OK')
        props.finishSubmit()
      },
      okText: '退出',
      onCancel() {
        console.log('Cancel')
      },
      centered: true
    })
  }
  return (
    <div className={styles.issue_comment_wrap}>
      <div className={styles.top_title}>
        <Icon
          type="close"
          style={{ fontSize: '16px', color: '#007fff' }}
          onClick={() => {
            console.log('showConfirm')
            if (!word) {
              props.finishSubmit()
            } else {
              showConfirm()
            }
          }}
        />
        <span>发评论</span>
        <span
          className={word ? `${styles.send} ${styles.send_active}` : styles.send}
          onClick={() => {
            submitComment()
          }}
        >
          发送
        </span>
      </div>
      <TextArea
        placeholder="回复:发表评论"
        ref={ref as any}
        autosize={true}
        value={word}
        onChange={e => {
          setword(e.target.value)
        }}
        className={styles.comment_text}
        onPressEnter={() => {
          submitComment()
        }}
      />
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  publishedFile: state.article.publishedFile,
  articleFileContent: state.article.articleFileContent
})

const mapDispatchToProps = (dispatch: any) => ({
  getArticleFileById: dispatch.article.getArticleFileById,
  getPublishedFile: dispatch.article.getPublishedFile,
  UpdateArticleThumbsUpNumById: dispatch.article.UpdateArticleThumbsUpNumById,
  createComment: dispatch.comment.createComment
})
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(IssueComment)
)
