const proxy = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    proxy('/api', {
      // target: 'http://artwrite.cn:3030/',
      // target: 'http://article-api.yrbing.com.cn/',
      target: 'http://127.0.0.1:3030',
      changeOrigin: true,
      pathRewrite: {
        '^/api': ''
      }
    })
  )
}
