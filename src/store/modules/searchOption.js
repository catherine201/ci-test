const defaultPagination = {
  defaultCurrent: 1,
  defaultPageSize: 10,
  current: 1,
  total: 0
}

const searchOptionState = {
  state: {
    pagination:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).searchOption &&
        JSON.parse(sessionStorage.getItem('store')).searchOption.pagination) ||
      defaultPagination
  },
  reducers: {
    setPagination(state, data) {
      return {
        ...state,
        pagination: data
      }
    }
  },
  effects: dispatch => ({
    getPagination(obj) {
      dispatch.searchOption.setPagination(obj)
    },
    resetPagination() {
      dispatch.searchOption.setPagination(defaultPagination)
    }
  })
}

export default searchOptionState
