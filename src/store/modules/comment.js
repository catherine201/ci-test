import createApi from '../../api/comment'
import store from '../index'

const comment = {
  state: {
    commentList:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).comment &&
        JSON.parse(sessionStorage.getItem('store')).comment.userList) ||
      []
  },
  reducers: {
    setCommentList(state, data) {
      return {
        ...state,
        commentList: data
      }
    }
  },
  effects: dispatch => ({
    // 获取评论
    async getComments(sendObj = {}) {
      const obj = {
        limit: sendObj.limit || 10,
        offset:
          sendObj.offset !== undefined
            ? sendObj.offset
            : (store.getState().searchOption.pagination.current - 1) * 10 || 0
      }
      if (sendObj.type) {
        obj.type = sendObj.type
      }
      if (sendObj.articleId) {
        obj.articleId = sendObj.articleId
      }
      const res = await createApi.getComments(obj)
      if (res && res.status === 200) {
        const result = JSON.parse(JSON.stringify(res))
        result.init = true
        dispatch.comment.setCommentList(result)
        const pager = {
          current: result.data.page.offset ? Math.floor(result.data.page.offset / 10) + 1 : 1,
          total: result.data.page.total
        }
        dispatch.searchOption.getPagination(pager)
        return new Promise(resolve => resolve(result))
      }
    },
    // 删除评论
    async deleteComment(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        id: sendObj.id
      }
      const res = await createApi.deleteComment(obj)
      if (res && res.status === 200) {
        const queryObj = {
          limit: store.getState().comment.commentList.data.page.limit,
          offset: store.getState().comment.commentList.data.page.offset
        }
        if (sendObj.type) {
          queryObj.type = sendObj.type
        }
        if (sendObj.articleId) {
          queryObj.articleId = sendObj.articleId
        }
        dispatch.comment.getComments(queryObj)
      }
    },
    // 创建评论
    async createComment(sendObj = {}) {
      const obj = {
        comment: sendObj.comment,
        openid: store.getState().account.userInfo.openid
      }
      if (sendObj.pid) {
        obj.pid = sendObj.pid
      }
      if (sendObj.type) {
        obj.type = sendObj.type
      }
      if (sendObj.articleId) {
        obj.articleId = sendObj.articleId
      }
      // 新建评论
      const res = await createApi.createComment(obj)
      if (res.status === 200) {
        // 新建成功就查询
        const againObj = {}
        if (sendObj.type) {
          againObj.type = sendObj.type
        }
        if (sendObj.articleId) {
          againObj.articleId = sendObj.articleId
        }
        return new Promise(resolve => resolve(dispatch.comment.getComments(againObj)))
      }
    }
  })
}

export default comment
