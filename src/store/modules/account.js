import createApi from '../../api/registerAndLogin'
import store from '../index'
// import constant from '@/utils/constant.js'

const account = {
  state: {
    isLogin:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).account &&
        JSON.parse(sessionStorage.getItem('store')).account.isLogin) ||
      false,
    userInfo:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).account &&
        JSON.parse(sessionStorage.getItem('store')).account.userInfo) ||
      {},
    userList:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).account &&
        JSON.parse(sessionStorage.getItem('store')).account.userList) ||
      {},
    userSearchWord:
      (sessionStorage.getItem('store') &&
        JSON.parse(sessionStorage.getItem('store')).account &&
        JSON.parse(sessionStorage.getItem('store')).account.userSearchWord) ||
      ''
  },
  reducers: {
    setIsLogin(state, data) {
      return {
        ...state,
        isLogin: data
      }
    },
    setUserInfo(state, data) {
      return {
        ...state,
        userInfo: data
      }
    },
    setUserList(state, data) {
      return {
        ...state,
        userList: data
      }
    },
    setUserSearchWord(state, data) {
      return {
        ...state,
        userSearchWord: data
      }
    }
  },
  effects: dispatch => ({
    async setIsLoginAction(val) {
      dispatch.account.setIsLogin(val)
    },
    async setUserInfoAction(obj) {
      dispatch.account.setUserInfo(obj)
    },
    async UpdateUserInfo(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        avator: sendObj.avator,
        nickName: sendObj.nickName
      }
      const res = await createApi.UpdateUserInfo(obj)
      if (res && res.status === 200) {
        let result = JSON.parse(JSON.stringify(store.getState().account.userInfo))
        result.avator = res.data.avator
        result.nickName = res.data.nickName
        dispatch.account.setUserInfo(result)
      }
    },
    // 更改密码
    async UpdatePassword(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        oldPassword: sendObj.oldPassword,
        newPassword: sendObj.newPassword,
        confirmPassword: sendObj.confirmPassword
      }
      const res = await createApi.UpdatePassword(obj)
      if (res && res.status === 200) {
        return new Promise(resolve => resolve())
      }
    },
    // 获取全部用户
    async GetAllUser(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        limit: sendObj.limit || 10,
        offset:
          sendObj.offset !== undefined
            ? sendObj.offset
            : (store.getState().searchOption.pagination.current - 1) * 10 || 0
      }
      if (store.getState().account.userSearchWord) {
        obj.word = store.getState().account.userSearchWord
      }
      const res = await createApi.GetAllUser(obj)
      if (res && res.status === 200) {
        const result = JSON.parse(JSON.stringify(res))
        result.init = true
        dispatch.account.setUserList(result)
        const pager = {
          current: Math.floor(result.data.page.offset / 10) + 1,
          total: result.data.page.total
        }
        dispatch.searchOption.getPagination(pager)
        return new Promise(resolve => resolve(result))
      }
    },
    // 更新用户权限
    async UpdateType(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        userOpenId: sendObj.userOpenId,
        admin: sendObj.admin
      }
      const res = await createApi.UpdateType(obj)
      if (res && res.status === 200) {
        const queryObj = {
          limit: store.getState().account.userList.data.page.limit,
          offset: store.getState().account.userList.data.page.offset
        }
        dispatch.account.GetAllUser(queryObj)
      }
    },
    // 删除用户
    async DeleteUserByOpenId(sendObj = {}) {
      const obj = {
        openid: store.getState().account.userInfo.openid,
        name: sendObj.name
      }
      const res = await createApi.DeleteUserByName(obj)
      if (res && res.status === 200) {
        const queryObj = {
          limit: store.getState().account.userList.data.page.limit,
          offset: store.getState().account.userList.data.page.offset
        }
        dispatch.account.GetAllUser(queryObj)
      }
    },
    // 用户管理的根据用户名搜索
    setUserSearchWordAction(val) {
      dispatch.account.setUserSearchWord(val)
      return new Promise(resolve => resolve())
    }
  })
}

export default account
