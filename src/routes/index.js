// import React from 'react'
import Loadable from 'react-loadable'
import { handleRoute } from '@/utils/index.js'
import models from './modules'
import Loading from '../views/loading/index'

// const Loading = () => <div>Loading...</div>
const page = name =>
  Loadable({
    loader: () => import(`../views/${name}`),
    loading: Loading
    // delay: 200,
    // timeout: 10000
  })

const Layout = Loadable({
  loader: () => import(`@/components/layout.tsx`),
  loading: Loading
})

const settingLayout = Loadable({
  loader: () => import(`@/components/setting/settingWrap.tsx`),
  loading: Loading
})

const routeConfig = [
  { path: '/', exact: true, strict: true, component: page('Dashboard/index.tsx'), auth: false },
  {
    path: '/setting',
    component: settingLayout,
    childRoutes: [
      { path: '/user', component: page('setting/user/index.tsx'), exact: true, animate: false },
      { path: '/changePsw', component: page('setting/changePsw/index.tsx'), exact: true, animate: false },
      {
        path: '/basic',
        component: page('setting/basic/index.tsx'),
        exact: true
      }
    ]
  },
  {
    path: '/verify',
    component: Layout,
    childRoutes: [{ path: '/article', component: page('articleVerify/index.tsx'), exact: true, animate: false }]
  },
  // 关于博主
  {
    path: '/aboutMe',
    component: page('aboutMe/index.tsx'),
    exact: true,
    animate: false,
    auth: false
  },
  // 赞助作者
  {
    path: '/Sponsor',
    component: page('Sponsor/index.tsx'),
    exact: true,
    animate: false,
    auth: false
  },
  // 给我留言
  {
    path: '/leaveMsg',
    component: page('message/index.tsx'),
    exact: true,
    animate: false,
    auth: false
  },
  {
    path: '/art',
    component: Layout,
    auth: false,
    childRoutes: [
      ...models,
      // 具体文章内容
      { path: '/articleContent/:id', component: page('articleContent/index.tsx'), exact: true, auth: false },
      // 根据每个用户进行查询文章列表
      { path: '/personalArt/:openid/:name', component: page('personalArt/index.tsx'), exact: true, auth: false },
      // 根据文集进行查询文章列表
      { path: '/anthologyArt/:pathId', component: page('anthology/index.tsx'), exact: true, auth: false }
    ]
  },
  // 生活笔记
  {
    path: '/aboutLife/:pathId?',
    // component: Layout,
    auth: false,
    animate: false,
    component: page('anthology/index.tsx')
    // childRoutes: [
    //   { path: '/articleContent/:id', component: page('articleContent/index.tsx'), exact: true, auth: false },
    //   { path: '/personalArt/:openid/:name', component: page('personalArt/index.tsx'), exact: true, auth: false }
    // ]
  },
  // 技术杂谈
  {
    path: '/aboutTech/:pathId?',
    // component: Layout,
    auth: false,
    animate: false,
    component: page('anthology/index.tsx')
    // childRoutes: [
    //   { path: '/articleContent/:id', component: page('articleContent/index.tsx'), exact: true, auth: false },
    //   { path: '/personalArt/:openid/:name', component: page('personalArt/index.tsx'), exact: true, auth: false }
    // ]
  },
  {
    path: '/article/notebooks',
    component: page('article'),
    exact: true
  },
  {
    path: '/article/notebooks/:folder',
    component: page('article'),
    exact: true,
    animate: false
  },
  {
    path: '/article/notebooks/:folder/:notes',
    component: page('article'),
    exact: true,
    animate: false
  },
  {
    path: '/article/notebooks/:folder/:notes/:file',
    component: page('article'),
    exact: true,
    animate: false
  },
  {
    // path: '/login/:flag?',
    path: '/login',
    component: page('login/index.tsx'),
    exact: true,
    auth: false
  },
  {
    path: '/me',
    component: page('Mine/index.tsx'),
    exact: true,
    auth: false
  },
  {
    path: '/register',
    component: page('register/index.tsx'),
    exact: true,
    auth: false
  },
  {
    path: '/loading',
    component: page('loading/index.tsx'),
    exact: true,
    auth: false
  }
]

export default handleRoute(routeConfig)
