import createApi from '../createApi'

const config = {
  // 创建评论
  createComment: {
    url: '/comment/createComment',
    options: {
      method: 'POST', // optional
      showLoading: false
    }
  },
  // 查询评论
  getComments: {
    url: '/comment/getComments',
    options: {
      showLoading: false // optional
    }
  },
  // 删除评论
  deleteComment: {
    url: '/comment/deleteComment',
    options: {
      method: 'POST' // optional
    }
  }
}

export default createApi(config)
