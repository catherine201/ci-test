function resolveIp() {
  const mode = process.env.NODE_ENV.trim();
  if (mode === "development") {
    return "/api";
  }
  // return 'http://article-api.yrbing.com.cn/'
  return "http://49.235.157.79:3030/";
}
export const serverIp = resolveIp();
