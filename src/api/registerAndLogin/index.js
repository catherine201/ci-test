import createApi from '../createApi'

const config = {
  // 注册
  register: {
    url: '/account/register',
    options: {
      method: 'POST' // optional
    }
  },
  // 登录
  login: {
    url: '/account/login',
    options: {
      method: 'POST' // optional
    }
  },
  // 更新用户信息
  UpdateUserInfo: {
    url: '/account/UpdateUserInfo',
    options: {
      method: 'POST' // optional
    }
  },
  // 更新密码
  UpdatePassword: {
    url: '/account/UpdatePassword',
    options: {
      method: 'POST' // optional
    }
  },
  // 获取所有用户
  GetAllUser: {
    url: '/account/GetAllUser',
    options: {
      showLoading: false
    }
  },
  SearchUserByName: {
    url: '/account/SearchUserByName',
    options: {
      showLoading: false
    }
  },
  // 更新权限
  UpdateType: {
    url: '/account/UpdateType',
    options: {
      method: 'POST'
    }
  },
  // 删除用户
  DeleteUserByName: {
    url: '/account/DeleteUserByName',
    options: {
      method: 'DELETE'
    }
  }
}

export default createApi(config)
