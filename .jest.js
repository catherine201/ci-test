module.exports = {
  setupFiles: [
    // 启动文件
    './tests/setUpTests.js',
    'jest-canvas-mock'
  ],
  setupFilesAfterEnv: ['./tests/setupAfterEnv.js'],
  // testEnvironment: 'node',  默认是jsdom
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  testPathIgnorePatterns: ['/node_modules/'],
  testRegex: '.*\\.test\\.(j|t)sx?$',
  collectCoverage: false, // 是否生成测试报告
  verbose: true, // 是否展示每条测试case 的结果
  silent: false, // 是否展示console.log 打印的东西
  lastCommit: false, // 是否只执行上次commit  文件
  collectCoverageFrom: [
    // 需要进行测试的文件集
    'src/utils/index.js'
  ],
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0
    },
    'src/utils/index.js': {
      branches: 2
    }
  },
  moduleNameMapper: {
    '^@/(.*).less': 'identity-obj-proxy',
    '^@/(.*)': '<rootDir>/src/$1', // 在测试js 文件中使用的别名
    '^react-tests/(.*)': '<rootDir>/tests/$1',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/tests/__mocks__/fileMock.js',
    '\\.(css|less|scss)$': 'identity-obj-proxy'
  },
  transform: {
    '^.+\\.(js|jsx)$': 'babel-jest',
    '^.+\\.(ts|tsx)$': 'ts-jest',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/tests/fileTransformer.js'
  },
  snapshotSerializers: ['enzyme-to-json/serializer'],
  globals: {
    // '$': function(){},
    // 'jsbarcode': {}
    __webpack_require__: {}
  }
}
